package com.evgeny.echofm;

import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.presenter.ISchedulerPresenter;

import org.joda.time.DateTimeComparator;

import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.BehaviorSubject;
import timber.log.Timber;


public class DayProgram {

    private ISchedulerPresenter schedulerPresenter;
    private LinkedList<Schedule> dayProgram;

    private BehaviorSubject<Schedule> scheduleNotifier;

    @Inject
    public DayProgram(ISchedulerPresenter schedulerPresenter) {
        this.schedulerPresenter = schedulerPresenter;
        this.scheduleNotifier = BehaviorSubject.create();
    }

    private void execute() {
        getSchedule().map(schedules -> {
            int currentProgramPosition = getCurrentProgramPosition(schedules);
            // notify about current program
            scheduleNotifier.onNext(schedules.get(currentProgramPosition));
            return getTimeUntilNextProgram(currentProgramPosition, schedules);
        }).flatMapSingle(delay -> Single.timer(delay, TimeUnit.MILLISECONDS)).subscribe(aLong -> {
            if (scheduleNotifier.hasObservers()) {
                // repeat
                execute();
            }
        }, throwable -> Timber.e(throwable, "day program execution error"));
    }

    public void subscribe(Consumer<? super Schedule> consumer) {
        if (!scheduleNotifier.hasObservers()) {
            execute();
        }
        scheduleNotifier.subscribe(consumer);
    }

    /**
     * send onComplete call to all subscribers
     */
    public void complete() {
        if (scheduleNotifier.hasObservers()) {
            scheduleNotifier.onComplete();
        }
    }

    private Maybe<LinkedList<Schedule>> getSchedule() {
        // check if the date already loaded
        if (dayProgram != null && DateTimeComparator.getDateOnlyInstance().compare(dayProgram.getFirst().approved_at, new Date()) == 0) {
            return Maybe.just(dayProgram);
        }

        return schedulerPresenter.getScheduleList()
                .map(schedules -> new LinkedList<>(Arrays.asList(schedules)))
                .doAfterSuccess(schedules -> dayProgram = schedules);
    }

    private int getCurrentProgramPosition(List<Schedule> schedules) {
        DateTimeComparator timeOnlyComparator = DateTimeComparator.getInstance();
        Date now = new Date();
        for (int i = schedules.size() - 1; i >= 0; i--) {
            Schedule schedule = schedules.get(i);
            if (timeOnlyComparator.compare(now, schedule.approved_at) > 0) {
                return i;
            }
        }

        // fallback to last program in the schedule
        return schedules.size() - 1;
    }

    private long getTimeUntilNextProgram(int currentProgramPosition, List<Schedule> schedules) {
        // TODO stop timer if the instance does not have any subscribers
        Schedule nextProgram;
        if (currentProgramPosition == schedules.size() - 1) {
            nextProgram = schedules.get(0);
        } else {
            nextProgram = schedules.get(currentProgramPosition + 1);
        }

        long time = nextProgram.approved_at.getTime() - System.currentTimeMillis();

        if (time >= 0) {
            return time;
        } else {
            return 3600000; // one hour
        }
    }
}
