package com.evgeny.echofm.utils;

import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Menu;
import android.view.MenuItem;

public final class DrawableUtils {

    /**
     * Tint all menu items
     *
     * @param menu menu
     * @param tint tint color
     */
    public static void tintMenuItems(Menu menu, int tint) {
        int itemSize = menu.size();
        if (itemSize <= 0) return;

        for (int i = 0; i < itemSize; i++) {
            MenuItem menuItem = menu.getItem(i);

            Drawable icon = menuItem.getIcon();
            if (icon == null) continue;

            icon = DrawableCompat.wrap(icon);
            DrawableCompat.setTint(icon, tint);
            menuItem.setIcon(icon);
        }
    }

    public static Drawable tintDrawable(Drawable drawable, int tint) {
        Drawable wrapDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrapDrawable, tint);

        return wrapDrawable;
    }
}
