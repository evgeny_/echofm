package com.evgeny.echofm.utils;

public final class TimeUtils {

    public static String makeShortTimeString(String durationFormat, long secs) {
        long hours, mins;

        hours = secs / 3600;
        secs %= 3600;
        mins = secs / 60;
        secs %= 60;

        return String.format(durationFormat, hours, mins, secs);
    }
}
