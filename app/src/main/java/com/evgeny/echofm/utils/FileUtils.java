package com.evgeny.echofm.utils;


import android.os.Environment;

import com.evgeny.echofm.proto.Broadcast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import okio.BufferedSink;
import okio.Okio;

public final class FileUtils {

    private static String ECHO_DIR_NAME = "echo";

//    public static Observable<File> saveFile(Response<ResponseBody> response, String fileName) {
//        return Observable.create(subscriber -> {
//            try {
//                // you can access headers of response
////                String header = response.headers().get("Content-Disposition");
//                // this is specific case, it's up to you how you want to save your file
//                // if you are not downloading file from direct link, you might be lucky to obtain file name from header
////                String fileName = header.replace("attachment; filename=", "");
//                // will create file in global Music directory, can be any other directory, just don't forget to handle permissions
//                File file = new File(getEchoStorageDir().getAbsoluteFile(), fileName);
//
//                BufferedSink sink = Okio.buffer(Okio.sink(file));
//                // you can access body of response
//                sink.writeAll(response.body().source());
//                sink.close();
//                subscriber.onNext(file);
//                subscriber.onCompleted();
//            } catch (IOException e) {
//                Timber.e(e, "save file named = %s failed", fileName);
//                subscriber.onError(e);
//            }
//        });
//    }

    /**
     * remove all files from public echo dir
     *
     * @return true if success, false otherwise
     */
    public static boolean cleanEchoDirectory() {
        boolean result = true;
        File echoDir = getEchoStorageDir();

        if (echoDir.isDirectory()) {
            String[] files = echoDir.list();
            for (String fileName : files) {
                boolean deleted = new File(echoDir, fileName).delete();
                result &= deleted;
            }
        }

        return result;
    }

    /**
     * Remove single record(file) by name.
     *
     * @param fileName file name on data storage
     * @return true if file was deleted.
     */
    public static boolean deleteFile(String fileName) {
        File echoDir = getEchoStorageDir();
        return new File(echoDir, fileName).delete();
    }

    /**
     * Get echo dir
     *
     * @return file path to echo dir
     */
    private static File getEchoStorageDir() {
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC), ECHO_DIR_NAME);
        if (!file.exists()) file.mkdir();

        return file;
    }

    /**
     * Check if file on storage exist
     *
     * @param path file path
     * @return true if exist, false otherwise
     */
    public static boolean isFileExist(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static void writeBroadcast(Broadcast broadcast) {
        File file = new File(getEchoStorageDir().getAbsoluteFile(), "fileName");
        try {
            BufferedSink sink = Okio.buffer(Okio.sink(file));
            broadcast.encode(sink);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
