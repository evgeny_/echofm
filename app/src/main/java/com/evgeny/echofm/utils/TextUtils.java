package com.evgeny.echofm.utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.widget.TextView;

import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Person;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Text utils methods.
 */

public final class TextUtils {

    private static Pattern imageSrcPattern = Pattern.compile("src=\"[^\"]*\"", Pattern.CASE_INSENSITIVE);

    /**
     * @param activity activity(context)
     * @param text     text to share
     */
    public static void shareText(Activity activity, String text) {
        Intent sharingIntent = ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setText(text)
                .getIntent();

        activity.startActivity(Intent.createChooser(sharingIntent, activity.getString(R.string.setting_share_app_intent_title)));
    }

    /**
     * Get selected text from text view.
     *
     * @param textView text view to get text selected from
     * @return selected text
     */
    public static CharSequence getSelectedText(TextView textView) {
        int min = 0;
        int max = textView.getText().length();
        if (textView.isFocused()) {
            final int selStart = textView.getSelectionStart();
            final int selEnd = textView.getSelectionEnd();

            min = Math.max(0, Math.min(selStart, selEnd));
            max = Math.max(0, Math.max(selStart, selEnd));
        }

        return textView.getText().subSequence(min, max);
    }

    /**
     * Create comma separated person list as a string
     *
     * @param persons person array
     * @return person array as a list
     */
    public static String personList(Person[] persons) {
        if (persons == null) return "";
        return android.text.TextUtils.join(", ", persons);
    }

    public static String personList(List<com.evgeny.echofm.proto.Person> persons) {
        if (persons == null || persons.isEmpty()) return "";

        StringBuilder personList = new StringBuilder();
        for (com.evgeny.echofm.proto.Person person : persons) {
            personList.append(person.i).append(" ").append(person.f).append(", ");
        }

        personList.delete(personList.length() - 2, personList.length() - 1);
        return personList.toString();
    }

    /**
     * Retrieve img url embedded in html img tag
     *
     * @param imgTag text with img tag inside
     * @return img url
     */
    public static String getUrlFromImgTag(String imgTag) {
        String url = null;

        Matcher m = imageSrcPattern.matcher(imgTag);
        if (m.find()) {
            url = imgTag.substring(m.start() + 5, m.end() - 1);
        }

        return url;
    }
}
