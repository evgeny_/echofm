package com.evgeny.echofm.utils;

import java.lang.reflect.Array;

/**
 * A part of GUAVA ObjectArrays
 * s. https://github.com/google/guava/blob/master/guava/src/com/google/common/collect/ObjectArrays.java
 */

public final class ArrayUtils {

    /**
     * Returns a new array of the given length with the specified component type.
     *
     * @param type the component type
     * @param length the length of the new array
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] newArray(Class<T> type, int length) {
        return (T[]) Array.newInstance(type, length);
    }

    /**
     * Returns a new array that contains the concatenated contents of two arrays.
     *
     * @param first the first array of elements to concatenate
     * @param second the second array of elements to concatenate
     * @param type the component type of the returned array
     */
    public static <T> T[] concat(T[] first, T[] second, Class<T> type) {
        T[] result = newArray(type, first.length + second.length);
        System.arraycopy(first, 0, result, 0, first.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

}
