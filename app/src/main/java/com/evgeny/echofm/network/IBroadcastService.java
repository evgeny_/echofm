package com.evgeny.echofm.network;

import com.evgeny.echofm.proto.Broadcast;

import io.reactivex.Maybe;

/**
 * Request broadcast and broadcast details from remote source
 */
public interface IBroadcastService {
    /**
     * Fetch all broadcast
     *
     * @return broadcast list
     */
    Maybe<Broadcast[]> getFirstPage();

    /**
     * Fetch next page of broadcast feed
     *
     * @param lastShowedBroadcast last broadcast on prevous broadcast feed page
     * @return broadcast list
     */
    Maybe<Broadcast[]> getNextPage(int lastShowedBroadcast);

    /**
     * Fetch broadcast details
     *
     * @param id broadcast id
     * @return founded broadcast
     */
    Maybe<Broadcast> get(long id);

    /**
     * Fetch all broadcast belong to given program
     *
     * @param programId echo program id
     * @return return all program broadcasts
     */
    Maybe<Broadcast[]> getArchive(String programId);
}
