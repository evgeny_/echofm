package com.evgeny.echofm.network;

import com.evgeny.echofm.proto.Broadcast;

import javax.inject.Inject;

import io.reactivex.Maybe;

public class BroadcastService implements IBroadcastService {

    private EchoApiService mService;

    @Inject
    public BroadcastService(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    @Override
    public Maybe<Broadcast[]> getFirstPage() {
        return mService.getBroadcasts("title,announce,approved_at,n_comments,n_views,n_likes,program,guests,hosts,has_audio,sound")
                .map(broadcastEchoApiResponse -> broadcastEchoApiResponse.content);
    }

    @Override
    public Maybe<Broadcast[]> getNextPage(int lastShowedBroadcast) {
        return mService.listBcastMore(lastShowedBroadcast, "title,announce,approved_at,n_comments,n_views,n_likes,program,guests,hosts,has_audio,sound")
                .map(broadcastEchoApiResponse -> broadcastEchoApiResponse.content);
    }

    @Override
    public Maybe<Broadcast> get(long id) {
        return mService.bcastDetail(id, "title,announce,approved_at,n_comments,n_views,n_likes,program,guests,hosts,has_text,body,has_audio,sound,url,approved_at")
                .map(broadcastEchoApiResponse -> broadcastEchoApiResponse.content[0]);
    }

    @Override
    public Maybe<Broadcast[]> getArchive(String programId) {
        return mService.searchBroadcast(programId, "title,announce,approved_at,n_comments,n_views,n_likes,program,guests,hosts,has_text,has_audio,sound,url,approved_at")
                .map(broadcastEchoApiResponse -> broadcastEchoApiResponse.content);
    }
}
