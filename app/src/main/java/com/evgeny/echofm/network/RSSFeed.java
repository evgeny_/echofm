package com.evgeny.echofm.network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "channel", strict = false)
public class RSSFeed {
    @Element
    public String title;
    //    @Element
//    public String link;
    @Element
    public String description;
    @Element
    public String language;
    @Element(required = false)
    public String copyright;
    @Element
    public String lastBuildDate;
    @ElementList(entry="item", inline=true)
    public List<FeedItem> entries;


//    public RSSFeed(String title, String link, String description, String language,
//                   String copyright, String pubDate) {
//        this.title = title;
//        this.link = link;
//        this.description = description;
//        this.language = language;
//        this.copyright = copyright;
//        this.pubDate = pubDate;
//    }
//
//    public List<FeedItem> getMessages() {
//        return entries;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public String getLink() {
//        return link;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public String getLanguage() {
//        return language;
//    }
//
//    public String getCopyright() {
//        return copyright;
//    }
//
//    public String getPubDate() {
//        return pubDate;
//    }
//
//    @Override
//    public String toString() {
//        return "Feed [copyright=" + copyright + ", description=" + description
//                + ", language=" + language + ", link=" + link + ", pubDate="
//                + pubDate + ", title=" + title + "]";
//    }
}
