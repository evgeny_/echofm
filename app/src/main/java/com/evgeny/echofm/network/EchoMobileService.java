package com.evgeny.echofm.network;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Service to get data from mobile portal
 * http://m.echo.msk.ru/
 */
public interface EchoMobileService {

    /**
     * Get blog page
     * @param id of blog entry
     * @return html response
     */
    @GET("blogs/detail.php")
    Observable<ResponseBody> getBlog(@Query("ID") String id);

}
