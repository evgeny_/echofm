package com.evgeny.echofm.network;

import com.evgeny.echofm.model.Comment;
import com.evgeny.echofm.model.News;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.proto.Broadcast;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EchoApiService {
    // NEWS

    /**
     * Get next news followed after news with id = {@code before}
     *
     * @param before last showed news id
     * @param fields news fields to request
     * @return next news list
     */
    @GET("/api/news.json")
    Maybe<EchoApiResponse<News>> getOlderNews(@Query("before") int before, @Query("fields") String fields);

    @GET("/api/news.json")
    Maybe<EchoApiResponse<News>> getLastNews(@Query("fields") String fields);

    @GET("/api/news/{id}.json")
    Maybe<EchoApiResponse<News>> newsDetail(@Path("id") String id, @Query("fields") String fields);

    // COMMENTS
    @GET("/api/comments.json")
    Maybe<EchoApiResponse<Comment>> comments(@Query("search[element_id]") String id, @Query("fields") String fields);

    // BROADCASTS
    @GET("/api/bcasts.json")
    Maybe<EchoApiResponse<Broadcast>> getBroadcasts(@Query("fields") String fields);

    @GET("/api/bcasts.json")
    Maybe<EchoApiResponse<Broadcast>> listBcastMore(@Query("before") int before, @Query("fields") String fields);

    @GET("/api/bcasts/{id}.json")
    Maybe<EchoApiResponse<Broadcast>> bcastDetail(@Path("id") long id, @Query("fields") String fields);

    @GET("/api/bcasts.json")
    Maybe<EchoApiResponse<Broadcast>> searchBroadcast(@Query("search[program_id]") String id, @Query("fields") String fields);

    // BLOGS
    @GET("/api/blogs.json")
    Call<EchoApiService> getBlogs(@Query("fields") String fields);

    @GET("/blog/rss.xml")
    Observable<RSSResponse> getBlogsXML();

    // SCHEDULE
    @GET("/api/scheds.json")
    Maybe<EchoApiResponse<Schedule>> getScheds(@Query("fields") String fields);

    // PROGRAM
    // /api/programs.json?page=0&search%5Bactive%5D=true&fields=title%2Chosts%2Cannounce%2Cpicture
    @GET("/api/programs.json")
    Observable<EchoApiResponse<Program>> getPrograms(@Query("page") int page,
                                                     @Query("search[active]") boolean isActive,
                                                     @Query("fields") String fields);

    @GET("/api/programs/{id}.json")
    Maybe<EchoApiResponse<Program>> getProgram(@Path("id") String id, @Query("fields") String fields);
}
