package com.evgeny.echofm.network;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "rss", strict = false)
public class RSSResponse {
    @Element
    public RSSFeed channel;

//    public RSSResponse(RSSFeed channel) {
//        this.channel = channel;
//    }
//
//    public RSSFeed getChannel() {
//        return channel;
//    }
}
