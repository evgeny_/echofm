package com.evgeny.echofm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Person implements Parcelable {
    public int id;
    public Avatar avatar;
    public boolean columnist;
    public boolean employee;
    public String f;
    public String i;
    public String o;
    public String position;
    public String type;
    public String url;

    public Person(Parcel in, ClassLoader loader) {
        id = in.readInt();
        avatar = in.readParcelable(loader);
        boolean[] bools = new boolean[2];
        in.readBooleanArray(bools);
        columnist = bools[0];
        employee = bools[1];

        String[] strings = new String[6];
        f = strings[0];
        i = strings[1];
        o = strings[2];
        position = strings[3];
        type = strings[4];
        url = strings[5];
    }

    public Person(Parcel in) {
        this(in, null);
    }

    @Override
    public String toString() {
        return i + " " + f;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeParcelable(avatar, flags);
        dest.writeBooleanArray(new boolean[]{columnist, employee});
        dest.writeStringArray(new String[]{f, i, o, position, type, url});
    }

//    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
//        public Person createFromParcel(Parcel in) {
//            return new Person(in);
//        }
//
//        public Person[] newArray(int size) {
//            return new Person[size];
//        }
//    };

    public static final Parcelable.Creator CREATOR = new Parcelable.ClassLoaderCreator() {

        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[0];
        }

        @Override
        public Person createFromParcel(Parcel source, ClassLoader loader) {
            return new Person(source, loader);
        }
    };
}
