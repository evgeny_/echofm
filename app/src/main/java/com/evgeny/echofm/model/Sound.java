package com.evgeny.echofm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Sound implements Parcelable {
    public String contentType;
    public int duration;
    public long length;
    public String type;
    public String url;

    public Sound(Parcel in) {
        String[] strings = new String[3];
        in.readStringArray(strings);
        contentType = strings[0];
        type = strings[1];
        url = strings[2];
        duration = in.readInt();
        length = in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                contentType, type, url
        });
        dest.writeInt(duration);
        dest.writeLong(length);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Sound createFromParcel(Parcel in) {
            return new Sound(in);
        }

        public Sound[] newArray(int size) {
            return new Sound[size];
        }
    };
}
