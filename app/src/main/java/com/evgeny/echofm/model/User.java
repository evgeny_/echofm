package com.evgeny.echofm.model;

public class User {
    public int id;
    public String name;
    public String url;
    public String fullName;
    public String type;
    public boolean blogger;
    public Avatar avatar;
    public int status;
}
