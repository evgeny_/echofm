package com.evgeny.echofm.model;

import java.util.Date;

public class News {
    public int id;
    public String type;
    public String title;
    public String body;
    public String announce;
    public Date approved_at;
    public int n_comments;
    public int n_views;
    public int n_likes;
    public boolean has_audio;
    public boolean has_video;
    public String url;
    public String news_sound;
    public boolean own;
    public String picture;
}
