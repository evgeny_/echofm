package com.evgeny.echofm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Avatar implements Parcelable {
    public String contentType;
    public int height;
    public int length;
    public String type;
    public String url;
    public int width;

    public Avatar(Parcel in) {
        String[] strings = new String[3];
        in.readStringArray(strings);
        contentType = strings[0];
        type = strings[1];
        url = strings[2];

        int[] ints = new int[3];
        in.readIntArray(ints);
        height = ints[0];
        length = ints[1];
        width = ints[2];
    }

    public Avatar() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                contentType, type, url
        });
        dest.writeIntArray(new int[]{
                height, length, width
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Avatar createFromParcel(Parcel in) {
            return new Avatar(in);
        }

        public Avatar[] newArray(int size) {
            return new Avatar[size];
        }
    };
}
