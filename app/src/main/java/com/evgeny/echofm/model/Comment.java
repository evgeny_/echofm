package com.evgeny.echofm.model;

import java.util.Date;

public class Comment {
    public String type;
    public int id;
    public boolean hide;
//    public boolean parentId;
    public String body;
    public User created_by;
    public int n_likes;
    public Date created_at;
}
