package com.evgeny.echofm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.Date;

public class Broadcast implements Parcelable {
    public String announce;
    public Date approved_at;
    public String body;
    private JsonElement guests; // may be a array or boolean value
    public boolean has_audio;
    public boolean has_text;
    public boolean has_video;
    private JsonElement hosts;  // may be a array or boolean value
    public int id;
    public boolean interactive;
    public int n_actions;
    public int n_comments;
    public int n_likes;
    public int n_views;
    public Program program;
    public Sound[] sound;
    public String title;
    public String type;
    public String url;
    public boolean isDownloaded = false;
    public String filePath;

    public Broadcast(Parcel in) {
        this(in, null);
    }

    public Person[] getGuests() {
        if (guests.isJsonArray()) {
            return new Gson().fromJson(guests, Person[].class);
        } else {
            return new Person[0];
        }
    }

    public Person[] getHosts() {
        if (hosts.isJsonArray()) {
            return new Gson().fromJson(hosts, Person[].class);
        } else {
            return new Person[0];
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{announce, title, type, url});
        dest.writeSerializable(approved_at);
        dest.writeTypedArray(getGuests(), flags);
        dest.writeTypedArray(getHosts(), flags);
    }

//    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
//        public Broadcast createFromParcel(Parcel in) {
//            return new Broadcast(in);
//        }
//
//        public Broadcast[] newArray(int size) {
//            return new Broadcast[size];
//        }
//    };

    public static final Parcelable.Creator CREATOR = new Parcelable.ClassLoaderCreator() {

        @Override
        public Broadcast createFromParcel(Parcel in) {
            return new Broadcast(in);
        }

        @Override
        public Broadcast[] newArray(int size) {
            return new Broadcast[0];
        }

        @Override
        public Broadcast createFromParcel(Parcel source, ClassLoader loader) {
            return new Broadcast(source, loader);
        }
    };

    private Broadcast(Parcel in, ClassLoader loader) {
        String[] strings = new String[4];
        in.readStringArray(strings);
        announce = strings[0];
        title = strings[1];
        type = strings[2];
        url = strings[3];

        Date date = (Date) in.readSerializable();

        Person[] guests = (Person[]) in.createTypedArray(Person.CREATOR);
        Person[] hosts = (Person[]) in.createTypedArray(Person.CREATOR);
//        guests = new Gson().toJsonTree(in.readParcelableArray(null), Person.class);
//        hosts = new Gson().toJsonTree(in.readParcelableArray(null), Person.class);
    }
}
