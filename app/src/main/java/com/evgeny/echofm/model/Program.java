package com.evgeny.echofm.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class Program implements Parcelable {
    public int id;
    public String announce;
    public String title;
    public String type;
    public String url;
    public Avatar picture;
    public String archived;
    public String schedule;
    private JsonElement hosts;
    private JsonElement guests;

    public Program(Parcel in) {
        id = in.readInt();
        String[] strings = new String[4];
        in.readStringArray(strings);
        announce = strings[0];
        title = strings[1];
        type = strings[2];
        url = strings[3];
    }

    public Program() {

    }

    public Person[] getHosts() {
        if (hosts != null && hosts.isJsonArray()) {
            return new Gson().fromJson(hosts, Person[].class);
        } else {
            return new Person[0];
        }
    }

    public Person[] getGuests() {
        if (guests != null && guests.isJsonArray()) {
            return new Gson().fromJson(guests, Person[].class);
        } else {
            return new Person[0];
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeStringArray(new String[]{
                announce, title, type, url
        });
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Program createFromParcel(Parcel in) {
            return new Program(in);
        }

        public Program[] newArray(int size) {
            return new Program[size];
        }
    };
}
