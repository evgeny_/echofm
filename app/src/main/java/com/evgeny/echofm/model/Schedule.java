package com.evgeny.echofm.model;

import java.util.Date;

public class Schedule {
    public String type;
    public int id;
    public Date approved_at;
    public String title;
    public boolean section;
    public Program program;
    public Person person;
    public Broadcast bcast;

}
