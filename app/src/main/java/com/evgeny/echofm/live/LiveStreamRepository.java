package com.evgeny.echofm.live;

import java.util.List;

public interface LiveStreamRepository {
    void persistSelected(LiveStream liveStream);
    LiveStream retrieveSelected();

    List<LiveStream> getSourceList();
}
