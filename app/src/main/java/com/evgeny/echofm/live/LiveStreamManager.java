package com.evgeny.echofm.live;

import java.util.List;

import timber.log.Timber;

public final class LiveStreamManager {

    private final LiveStreamRepository streamRepo;
    private List<LiveStream> liveStreams;

    public LiveStreamManager(LiveStreamRepository liveStreamRepository) {
        this.streamRepo = liveStreamRepository;
    }

    public List<LiveStream> getStreams() {
        if (liveStreams != null) return liveStreams;

        liveStreams = streamRepo.getSourceList();
        return liveStreams;
    }

    public LiveStream getSelectedStream() {
        return streamRepo.retrieveSelected();
    }

    public CharSequence[] getNameArray() {
        List<LiveStream> streams = getStreams();
        CharSequence[] names = new CharSequence[streams.size()];
        for (int i = 0; i < streams.size(); i++) {
            names[i] = streams.get(i).getName();
        }

        return names;
    }

    public void memorizeStream(LiveStream stream) {
        Timber.d("select live stream: %s", stream);
        streamRepo.persistSelected(stream);
    }
}

