package com.evgeny.echofm.live;

import android.content.Context;
import android.content.SharedPreferences;

import com.evgeny.echofm.R;

import java.util.ArrayList;
import java.util.List;

public class EchoLiveStreamRepository implements LiveStreamRepository {

    private final Context context;
    private final SharedPreferences sharedPreferences;

    public EchoLiveStreamRepository(Context context, SharedPreferences sharedPreferences) {
        this.context = context;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void persistSelected(LiveStream liveStream) {
        sharedPreferences.edit().putInt("selected_stream", liveStream.getId()).apply();
    }

    @Override
    public LiveStream retrieveSelected() {
        int id = sharedPreferences.getInt("selected_stream", 0);

        return getSourceList().get(id);
    }

    @Override
    public List<LiveStream> getSourceList() {
        String[] names = context.getResources().getStringArray(R.array.stream_name);
        String[] urls = context.getResources().getStringArray(R.array.stream_url);

        if (names.length != urls.length)
            throw new IllegalStateException("the urls array and names array are not of the same length");

        List<LiveStream> streams = new ArrayList<>();
        for (int i = 0; i < names.length; i++) {
            streams.add(new LiveStream(i, urls[i], names[i]));
        }

        return streams;
    }
}
