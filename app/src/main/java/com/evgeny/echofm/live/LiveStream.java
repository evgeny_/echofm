package com.evgeny.echofm.live;

import java.util.Objects;

public final class LiveStream {

    private final int id;
    private final String url;
    private final String name;

    LiveStream(int id, String url, String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        int result = 11;

        result = 31 * result + id;
        result = 31 * result + url.hashCode();
        result = 31 * result + name.hashCode();

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof LiveStream)) return false;
        LiveStream ls = (LiveStream) obj;

        return id == ls.id && Objects.equals(url, ls.url) && Objects.equals(name, ls.name);
    }

    @Override
    public String toString() {
        return "id=" + id + "; name=" + name + "; url " + url;
    }
}
