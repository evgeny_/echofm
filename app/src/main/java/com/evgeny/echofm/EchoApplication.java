package com.evgeny.echofm;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.evgeny.echofm.injection.component.BlogComponent;
import com.evgeny.echofm.injection.component.NetComponent;
import com.evgeny.echofm.injection.module.AppModule;
import com.evgeny.echofm.injection.module.MobileEchoModule;
import com.evgeny.echofm.injection.module.NetModule;

import java.net.SocketTimeoutException;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

public class EchoApplication extends Application {
    private NetComponent mNetComponent;

    public static final String BASE_URL = "https://echo.msk.ru";
    private BlogComponent mBlogComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Configure.configStetho(this);

        // init crashlytics
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build();
        Fabric.with(this, crashlyticsKit);

        // init logger
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsLogger());
        }

        // init dagger component
        mNetComponent = com.evgeny.echofm.injection.component.DaggerNetComponent.builder()
                // list of modules that are part of this component need to be created here too
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL))
                .build();

        // enable vector drawables support
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    public NetComponent getNetComponent() {
        return mNetComponent;
    }

    /**
     * get blog component
     * @return blog component
     */
    public BlogComponent plusBlogComponent() {
        if (mBlogComponent == null) {
            mBlogComponent = mNetComponent.plusBlogComponent(new MobileEchoModule());
        }

        return mBlogComponent;
    }

    public void clearBlogComponent() {
        mBlogComponent = null;
    }


    /**
     * Collect logs to send with next crash report to crashlytics server.
     */
    private static class CrashlyticsLogger extends Timber.Tree {

        @Override
        protected void log(int priority, String tag, String message, Throwable t) {
            Crashlytics.log(priority, tag, message);

            if (t != null && !(t instanceof SocketTimeoutException) && priority == Log.ERROR) {
                Crashlytics.logException(t);
            }
        }
    }
}