package com.evgeny.echofm.injection.component;

import com.evgeny.echofm.injection.module.AppModule;
import com.evgeny.echofm.injection.module.DbModule;
import com.evgeny.echofm.injection.module.MobileEchoModule;
import com.evgeny.echofm.injection.module.NetModule;
import com.evgeny.echofm.service.PlayerService;
import com.evgeny.echofm.ui.blog.BlogListFragment;
import com.evgeny.echofm.ui.broadcast.BroadcastDetailActivity;
import com.evgeny.echofm.ui.broadcast.BroadcastDetailCommentsFragment;
import com.evgeny.echofm.ui.broadcast.BroadcastDownloadedActivity;
import com.evgeny.echofm.ui.broadcast.BroadcastListFragment;
import com.evgeny.echofm.ui.media.PlayerActivity;
import com.evgeny.echofm.ui.news.NewsDetailActivity;
import com.evgeny.echofm.ui.news.NewsListFragment;
import com.evgeny.echofm.ui.program.FavouriteProgramListActivity;
import com.evgeny.echofm.ui.program.ProgramArchiveFragment;
import com.evgeny.echofm.ui.program.ProgramDetailActivity;
import com.evgeny.echofm.ui.program.ProgramListActivity;
import com.evgeny.echofm.ui.schedule.OnAirActivity;
import com.evgeny.echofm.ui.schedule.ScheduleActivity;
import com.evgeny.echofm.ui.settings.SettingsActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class, DbModule.class})
public interface NetComponent {

    BlogComponent plusBlogComponent(MobileEchoModule mobileEchoModule);

    void inject(NewsListFragment newsListFragment);

    void inject(BroadcastListFragment broadcastListFragment);

    void inject(BlogListFragment blogListFragment);

    void inject(NewsDetailActivity newsDetailActivity);

    void inject(BroadcastDetailActivity broadcastDetailActivity);

    void inject(BroadcastDetailCommentsFragment broadcastDetailCommentsFragment);

    void inject(OnAirActivity onAirActivity);

    void inject(ScheduleActivity onAirActivity);

    void inject(PlayerService playerService);

    void inject(PlayerActivity playerActivity);

    void inject(ProgramArchiveFragment programArchiveFragment);

    void inject(FavouriteProgramListActivity favouriteProgramListActivity);

    void inject(ProgramDetailActivity programDetailActivity);

    void inject(ProgramListActivity programListActivity);

    void inject(SettingsActivity.GeneralPreferenceFragment generalPreferenceFragment);

    void inject(BroadcastDownloadedActivity broadcastDownloadedActivity);
}
