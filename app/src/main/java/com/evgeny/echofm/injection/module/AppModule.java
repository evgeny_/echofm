package com.evgeny.echofm.injection.module;

import android.app.Application;

import com.evgeny.echofm.presenter.ISchedulerPresenter;
import com.evgeny.echofm.presenter.SchedulerPresenter;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = AppModule.Declarations.class)
public final class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    /** bind abstract components here **/
    @Module
    public interface Declarations {
        @Binds
        ISchedulerPresenter bindSchedulePresenter(SchedulerPresenter schedulerPresenter);

//        @Binds
//        RecyclerView.Adapter bindBroadcastAdapter(BroadcastAdapter broadcastAdapter);
    }
}
