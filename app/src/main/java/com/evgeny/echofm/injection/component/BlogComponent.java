package com.evgeny.echofm.injection.component;

import com.evgeny.echofm.injection.module.MobileEchoModule;
import com.evgeny.echofm.injection.scope.MobileApiScope;
import com.evgeny.echofm.ui.blog.BlogDetailActivity;

import dagger.Subcomponent;

@MobileApiScope
@Subcomponent(modules = {MobileEchoModule.class})
public interface BlogComponent {

    public void inject(BlogDetailActivity blogDetailActivity);
}
