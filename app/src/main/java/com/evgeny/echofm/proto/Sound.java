// Code generated by Wire protocol buffer compiler, do not edit.
// Source file: broadcast.proto at 39:1
package com.evgeny.echofm.proto;

import com.squareup.wire.FieldEncoding;
import com.squareup.wire.Message;
import com.squareup.wire.ProtoAdapter;
import com.squareup.wire.ProtoReader;
import com.squareup.wire.ProtoWriter;
import com.squareup.wire.WireField;
import com.squareup.wire.internal.Internal;
import java.io.IOException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.StringBuilder;
import okio.ByteString;

public final class Sound extends Message<Sound, Sound.Builder> {
  public static final ProtoAdapter<Sound> ADAPTER = new ProtoAdapter_Sound();

  private static final long serialVersionUID = 0L;

  public static final String DEFAULT_URL = "";

  public static final Integer DEFAULT_LENGHT = 0;

  public static final Integer DEFAULT_DURATION = 0;

  @WireField(
      tag = 1,
      adapter = "com.squareup.wire.ProtoAdapter#STRING"
  )
  public final String url;

  @WireField(
      tag = 2,
      adapter = "com.squareup.wire.ProtoAdapter#INT32"
  )
  public final Integer lenght;

  @WireField(
      tag = 3,
      adapter = "com.squareup.wire.ProtoAdapter#INT32"
  )
  public final Integer duration;

  public Sound(String url, Integer lenght, Integer duration) {
    this(url, lenght, duration, ByteString.EMPTY);
  }

  public Sound(String url, Integer lenght, Integer duration, ByteString unknownFields) {
    super(ADAPTER, unknownFields);
    this.url = url;
    this.lenght = lenght;
    this.duration = duration;
  }

  @Override
  public Builder newBuilder() {
    Builder builder = new Builder();
    builder.url = url;
    builder.lenght = lenght;
    builder.duration = duration;
    builder.addUnknownFields(unknownFields());
    return builder;
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) return true;
    if (!(other instanceof Sound)) return false;
    Sound o = (Sound) other;
    return unknownFields().equals(o.unknownFields())
        && Internal.equals(url, o.url)
        && Internal.equals(lenght, o.lenght)
        && Internal.equals(duration, o.duration);
  }

  @Override
  public int hashCode() {
    int result = super.hashCode;
    if (result == 0) {
      result = unknownFields().hashCode();
      result = result * 37 + (url != null ? url.hashCode() : 0);
      result = result * 37 + (lenght != null ? lenght.hashCode() : 0);
      result = result * 37 + (duration != null ? duration.hashCode() : 0);
      super.hashCode = result;
    }
    return result;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    if (url != null) builder.append(", url=").append(url);
    if (lenght != null) builder.append(", lenght=").append(lenght);
    if (duration != null) builder.append(", duration=").append(duration);
    return builder.replace(0, 2, "Sound{").append('}').toString();
  }

  public static final class Builder extends Message.Builder<Sound, Builder> {
    public String url;

    public Integer lenght;

    public Integer duration;

    public Builder() {
    }

    public Builder url(String url) {
      this.url = url;
      return this;
    }

    public Builder lenght(Integer lenght) {
      this.lenght = lenght;
      return this;
    }

    public Builder duration(Integer duration) {
      this.duration = duration;
      return this;
    }

    @Override
    public Sound build() {
      return new Sound(url, lenght, duration, super.buildUnknownFields());
    }
  }

  private static final class ProtoAdapter_Sound extends ProtoAdapter<Sound> {
    ProtoAdapter_Sound() {
      super(FieldEncoding.LENGTH_DELIMITED, Sound.class);
    }

    @Override
    public int encodedSize(Sound value) {
      return (value.url != null ? ProtoAdapter.STRING.encodedSizeWithTag(1, value.url) : 0)
          + (value.lenght != null ? ProtoAdapter.INT32.encodedSizeWithTag(2, value.lenght) : 0)
          + (value.duration != null ? ProtoAdapter.INT32.encodedSizeWithTag(3, value.duration) : 0)
          + value.unknownFields().size();
    }

    @Override
    public void encode(ProtoWriter writer, Sound value) throws IOException {
      if (value.url != null) ProtoAdapter.STRING.encodeWithTag(writer, 1, value.url);
      if (value.lenght != null) ProtoAdapter.INT32.encodeWithTag(writer, 2, value.lenght);
      if (value.duration != null) ProtoAdapter.INT32.encodeWithTag(writer, 3, value.duration);
      writer.writeBytes(value.unknownFields());
    }

    @Override
    public Sound decode(ProtoReader reader) throws IOException {
      Builder builder = new Builder();
      long token = reader.beginMessage();
      for (int tag; (tag = reader.nextTag()) != -1;) {
        switch (tag) {
          case 1: builder.url(ProtoAdapter.STRING.decode(reader)); break;
          case 2: builder.lenght(ProtoAdapter.INT32.decode(reader)); break;
          case 3: builder.duration(ProtoAdapter.INT32.decode(reader)); break;
          default: {
            FieldEncoding fieldEncoding = reader.peekFieldEncoding();
            Object value = fieldEncoding.rawProtoAdapter().decode(reader);
            builder.addUnknownField(tag, fieldEncoding, value);
          }
        }
      }
      reader.endMessage(token);
      return builder.build();
    }

    @Override
    public Sound redact(Sound value) {
      Builder builder = value.newBuilder();
      builder.clearUnknownFields();
      return builder.build();
    }
  }
}
