package com.evgeny.echofm.download;

/**
 *
 */
public interface Target {
    //update progress
    public void updateProgress(DownloadableResult result);

    // reset target object to initial state
    public void reset();
}
