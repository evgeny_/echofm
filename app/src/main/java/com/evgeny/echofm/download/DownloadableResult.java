package com.evgeny.echofm.download;

/**
 * Created by anshul on 7/2/17.
 */

public class DownloadableResult {

    private int percent;
    private int downloadStatus;
    private String localUri;

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public int getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(int downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public void setLocalUri(String localUri) {
        this.localUri = localUri;
    }

    public String getLocalUri() {
        return localUri;
    }
}
