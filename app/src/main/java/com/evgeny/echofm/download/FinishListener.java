package com.evgeny.echofm.download;

/**
 * Created by ezino on 27/05/2017.
 */

public interface FinishListener {
    public void onFinished(DownloadableResult result);
}
