package com.evgeny.echofm.download;

import android.app.DownloadManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;

import static com.evgeny.echofm.download.Constants.DOWNLOAD_COMPLETE_PERCENT;

public class DownloadRequest {
//    private final static int STATUS_ACTIVE = 0;
//    private final static int STATUS_FINISHED = 1;
//    private final static int STATUS_FAILED = 2;

//    private final static String TAG = "DownloadRequest";

    private final static int DOWNLOAD_QUERY_DELAY_PARAM = 200;
    private final static int MIN_DOWNLOAD_PERCENT_DIFF = 3;
    private final static int PERCENT_MULTIPLIER = 100;

    private DownloadableResult lastResult;

    private final long downloadId;
    private final DownloadManager downloadManager;
    private int lastEmittedDownloadPercent = 0;
    private Target target;
    private FinishListener finishListener;

    public static class Builder {
        private final DownloadManager.Request downloadRequest;
        private DownloadManager dm;
        private Target target;
        private FinishListener fl;

        public Builder(DownloadManager dm, @NonNull Uri downloadUrl) {
            this.dm = dm;
            this.downloadRequest = new DownloadManager.Request(downloadUrl);
            this.downloadRequest.allowScanningByMediaScanner();
            this.downloadRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_MUSIC, "echo/" + downloadUrl.getLastPathSegment());
        }

        public Builder setNotificationTitle(CharSequence title) {
            downloadRequest.setTitle(title);

            return this;
        }

        public Builder setNotificationDescription(CharSequence description) {
            downloadRequest.setDescription(description);
            return this;
        }

        public Builder setTarget(Target target) {
            this.target = target;
            return this;
        }

        public Builder setOnFinishListener(FinishListener fl) {
            this.fl = fl;

            return this;
        }

        public DownloadRequest execute() {
            long downloadId = dm.enqueue(downloadRequest);
            return new DownloadRequest(downloadId, dm, target, fl);
        }
    }

    private DownloadRequest(long downloadId, DownloadManager downloadManager, Target target, FinishListener fl) {
        this.downloadId = downloadId;
        this.downloadManager = downloadManager;
        this.lastResult = new DownloadableResult();
        lastResult.setPercent(1);

        if (fl != null) {
            this.finishListener = fl;
        }

        if (target != null) {
            bindTarget(target);
        }
    }

    /**
     * bind this request to a new target and start progress query process
     *
     * @param target new target
     */
    public void bindTarget(Target target) {
        this.target = target;

        notifyProgressUpdate(lastResult);
        queryDownloadPercents();
    }

    /**
     * bind target will not get any notifications anymore
     * the progress query process stopped automatically
     */
    public void unbindTarget() {
        this.target = null;
    }

    public DownloadableResult getLastResult() {
        return lastResult;
    }

    private void queryDownloadPercents() {
        if (target == null) return;

        DownloadableResult downloadableResult = getDownloadResult(this.downloadManager, this.downloadId);
        if (downloadableResult == null) return;

        lastResult = downloadableResult;

        //Get the current DownloadPercent and download status
        int currentDownloadPercent = downloadableResult.getPercent();
        int downloadStatus = downloadableResult.getDownloadStatus();

        // notify progress change
        if ((currentDownloadPercent - lastEmittedDownloadPercent >= MIN_DOWNLOAD_PERCENT_DIFF) ||
                currentDownloadPercent == DOWNLOAD_COMPLETE_PERCENT) {
            notifyProgressUpdate(downloadableResult);
            lastEmittedDownloadPercent = currentDownloadPercent;
        }

        if (currentDownloadPercent == DOWNLOAD_COMPLETE_PERCENT) {
            notifyOnFinished(downloadableResult);
        }

        switch (downloadStatus) {
            case DownloadManager.STATUS_FAILED:
                break;
            case DownloadManager.STATUS_SUCCESSFUL:
                break;
            case DownloadManager.STATUS_PENDING:
            case DownloadManager.STATUS_RUNNING:
            case DownloadManager.STATUS_PAUSED:
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        queryDownloadPercents();
                    }
                }, DOWNLOAD_QUERY_DELAY_PARAM);
                break;

            case DownloadManager.ERROR_FILE_ERROR:
                break;
        }
    }

    /**
     * @param downloadManager - Android's DownloadManager
     * @param downloadId      - The downloadId for which the progress has to be fetched from the db.
     * @return last downloadable result
     */
    private DownloadableResult getDownloadResult(DownloadManager downloadManager, long downloadId) {

        //Create a query with downloadId as the filter.
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);

        //Create an instance of downloadable result
        DownloadableResult downloadableResult = new DownloadableResult();

        Cursor cursor = null;
        try {
            cursor = downloadManager.query(query);
            if (cursor == null || !cursor.moveToFirst()) {
                return downloadableResult;
            }
            //Get the download percent
            float bytesDownloaded =
                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
            float bytesTotal =
                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));
            int downloadPercent = (int) ((bytesDownloaded / bytesTotal) * PERCENT_MULTIPLIER);
            if (downloadPercent <= Constants.DOWNLOAD_COMPLETE_PERCENT) {
                downloadableResult.setPercent(downloadPercent);
            }
            //Get the download status
            int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
            int downloadStatus = cursor.getInt(columnIndex);

            downloadableResult.setDownloadStatus(downloadStatus);

            // locale store uri
            String path = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            downloadableResult.setLocalUri(path);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return downloadableResult;
    }

    private void notifyProgressUpdate(DownloadableResult result) {
        target.updateProgress(result);
    }

    private void notifyOnFinished(DownloadableResult result) {
        if (finishListener != null) {
            finishListener.onFinished(result);
        }
    }

    public void cancelDownload() {
        downloadManager.remove(downloadId);
    }

//    public void setOnFinishListener(FinishListener listener) {
//        finishListener = listener;
//    }
}
