package com.evgeny.echofm.presenter;

import android.content.ContentValues;
import android.text.TextUtils;

import com.evgeny.echofm.db.DownloadedBroadcast;
import com.evgeny.echofm.db.IBroadcastRepository;
import com.evgeny.echofm.network.EchoApiService;
import com.evgeny.echofm.network.IBroadcastService;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.utils.FileUtils;
import com.squareup.sqlbrite3.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class BroadcastPresenter {

    //    private EchoApiService mService;
//    private final BriteDatabase mDb;
//    private static final String SELECT_ALL = "SELECT * FROM " + DownloadedBroadcast.TABLE + " ORDER BY rowid DESC";
//    private static final String SELECT_BY_ECHO_ID = "SELECT * FROM " + DownloadedBroadcast.TABLE
//            + " WHERE " + DownloadedBroadcast.ECHO_ID + " = ? LIMIT 1";
    @Inject
    IBroadcastService broadcastService;

    @Inject
    IBroadcastRepository broadcastRepository;

    @Inject
    public BroadcastPresenter(EchoApiService echoApiService, BriteDatabase db) {
//        mService = echoApiService;
//        mDb = db;
    }

    /**
     * load bcast over net and map them to local db broadcasts. Add to broadcast items file path if any.
     *
     * @return broadcast observable
     */
    public Maybe<List<DownloadedBroadcast>> getBroadcasts() {
        return broadcastService.getFirstPage()
                .zipWith(getAllBroadcasts(), (broadcasts, downloadedBroadcasts) -> {
                    List<DownloadedBroadcast> db = new ArrayList<>(broadcasts.length);
                    for (Broadcast broadcast : broadcasts) {
                        db.add(convertBroadcast(broadcast, downloadedBroadcasts));
                    }

                    return db;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private DownloadedBroadcast convertBroadcast(Broadcast broadcast, List<DownloadedBroadcast> downloadedBroadcasts) {
        for (DownloadedBroadcast downloadedBroadcast : downloadedBroadcasts) {
            if (broadcast.id == downloadedBroadcast.echoId()) {
                return downloadedBroadcast;
            }
        }

        return new DownloadedBroadcast.Builder(broadcast.id).broadcast(broadcast.encode()).build();
    }

    public Maybe<List<DownloadedBroadcast>> getOlderBroadcasts(int lastShowedBroadcast) {
        return broadcastService.getNextPage(lastShowedBroadcast)
                .zipWith(getAllBroadcasts(), (broadcasts, downloadedBroadcasts) -> {
                    List<DownloadedBroadcast> db = new ArrayList<>(broadcasts.length);
                    for (Broadcast broadcast : broadcasts) {
                        db.add(convertBroadcast(broadcast, downloadedBroadcasts));
                    }

                    return db;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Get remote broadcast and merge it with db broadcast if any
     *
     * @param id echo id
     * @return merged broadcast
     */
    public Maybe<DownloadedBroadcast> getBroadcast(long id) {
        return broadcastService.get(id)
                .zipWith(getDownloadedBroadcast(id), (broadcast, downloadedBroadcasts) -> {
                    if (downloadedBroadcasts != null && broadcast.id == downloadedBroadcasts.echoId()) {
                        return downloadedBroadcasts;
                    } else {
                        return new DownloadedBroadcast.Builder(broadcast.id).broadcast(broadcast.encode()).build();
                    }
                });
    }

    /**
     * Persist broadcast to db.
     *
     * @param broadcast broadcast
     * @return new _id
     */
    public long persistDownloadedBroadcast(DownloadedBroadcast broadcast) {
        return broadcastRepository.add(broadcast);
    }

    /**
     * get all persisted bcasts from db
     *
     * @return observable which emit a bcast list
     */
    public Maybe<List<DownloadedBroadcast>> getAllBroadcasts() {
        return broadcastRepository.getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * get bcast with given echo id from db
     *
     * @param echoId bcast echo id
     * @return empty list or list with one item
     */
//    private Observable<List<DownloadedBroadcast>> getDbBroadcast(String echoId) {
//        return mDb.createQuery(DownloadedBroadcast.TABLE, SELECT_BY_ECHO_ID, echoId)
//                .mapToList(DownloadedBroadcast.MAPPER);
//    }
    public Maybe<DownloadedBroadcast> getDownloadedBroadcast(long echoId) {
        return broadcastRepository.get(echoId);
    }

    /**
     * Remove all rows from "download broadcast" table.
     */
    public void removeAllBroadcasts() {
        broadcastRepository.removeAll();
    }

    public DownloadedBroadcast removeBroadcast(DownloadedBroadcast broadcast) {
        if (!TextUtils.isEmpty(broadcast.filePath())) FileUtils.deleteFile(broadcast.filePath());
        broadcast.updateFilePath(null);
        broadcastRepository.remove(broadcast.echoId());
        return broadcast;
    }

    /**
     * Get program archive.
     *
     * @param id program id
     * @return observable
     */
    public Maybe<List<DownloadedBroadcast>> getBroadcastArchive(String id) {
        return broadcastService.getArchive(id)
                .zipWith(getAllBroadcasts(), (broadcasts, downloadedBroadcasts) -> {
                    List<DownloadedBroadcast> db = new ArrayList<>(broadcasts.length);
                    for (Broadcast broadcast : broadcasts) {
                        db.add(convertBroadcast(broadcast, downloadedBroadcasts));
                    }

                    return db;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     *
     * @param id broadcast echo id
     * @param position position to persist
     */
    public void savePosition(long id, long position) {
        ContentValues contentValues =
                new DownloadedBroadcast.ContentValueBuilder().timePlayed(position).build();
        broadcastRepository.update(id, contentValues);
    }
}
