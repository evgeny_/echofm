package com.evgeny.echofm.presenter;

import com.evgeny.echofm.model.News;
import com.evgeny.echofm.network.EchoApiService;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class NewsPresenter {
    EchoApiService mService;

    @Inject
    public NewsPresenter(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    public Maybe<News[]> getLastNews() {
        return mService.getLastNews("title,announce,approved_at,n_comments,n_views,n_likes")
                .map(newsEchoApiResponse -> newsEchoApiResponse.content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<News[]> getOlderNews(int newsId) {
        return mService.getOlderNews(newsId, "title,announce,approved_at,n_comments,n_views,n_likes")
                .map(newsEchoApiResponse -> newsEchoApiResponse.content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<News> getNewsDetail(String id) {
        return mService.newsDetail(id, "body,approved_at,title,url")
                .map(newsEchoApiResponse -> newsEchoApiResponse.content[0])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
