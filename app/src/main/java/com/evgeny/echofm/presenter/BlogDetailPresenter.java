package com.evgeny.echofm.presenter;

import com.evgeny.echofm.network.EchoMobileService;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import io.reactivex.Observable;
import timber.log.Timber;

public class BlogDetailPresenter {
    private final EchoMobileService mMobileService;
    private static final String TAG = "BlogDetailPresenter";

    @Inject
    public BlogDetailPresenter(EchoMobileService echoMobileService) {
        mMobileService = echoMobileService;
    }

    /**
     * Get single blog html document
     *
     * @param url blog url
     * @return html document
     */
    public Observable<Document> getBlogDetail(final URL url) {
        return Observable.defer(() -> Observable.just(getBlogId(url.getPath())))
                .flatMap(mMobileService::getBlog)
                .map(responseBody -> {
                    try {
                        return Jsoup.parse(responseBody.string());
                    } catch (IOException e) {
                        Timber.tag(TAG).e("cant get html for url=%s", url, e);
                        return null;
                    }
                });
    }

    private String getBlogId(final String urlPath) {
        String regexp = "^/.*?/.*?/(.*)-echo/$";

        Pattern pattern = Pattern.compile(regexp);

        Matcher m = pattern.matcher(urlPath);
        if (m.find()) {
            return m.group(1);
        } else {
            return null;
        }
    }
}
