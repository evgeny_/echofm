package com.evgeny.echofm.presenter;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;

import com.evgeny.echofm.db.FavouriteProgram;
import com.evgeny.echofm.model.Program;
import com.squareup.sqlbrite3.BriteDatabase;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;


public class FavouriteProgramPresenter {
    private final BriteDatabase mDb;

    private static final String EXIST_QUERY = "SELECT COUNT(*) FROM "
            + FavouriteProgram.TABLE
            + " WHERE "
            + FavouriteProgram.ECHO_ID
            + " = ?";

    private static final String DELETE_WHERE = FavouriteProgram.ECHO_ID + "=?";

    private static final String SELECT_ALL = "SELECT * FROM " + FavouriteProgram.TABLE;

    @Inject
    public FavouriteProgramPresenter(BriteDatabase db) {
        mDb = db;
    }

    public long insertProgram(Program program) {
        return mDb.insert(FavouriteProgram.TABLE, SQLiteDatabase.CONFLICT_IGNORE,
                new FavouriteProgram.Builder()
                .echoId(program.id)
                .title(program.title)
                .announce(program.announce)
                .hosts(Arrays.toString(program.getHosts()))
                .pictureUrl(program.picture == null ? null : program.picture.url)
                .build());
    }

    public long insertProtoProgram(com.evgeny.echofm.proto.Program program, List hosts) {
        return mDb.insert(FavouriteProgram.TABLE, SQLiteDatabase.CONFLICT_IGNORE, new FavouriteProgram.Builder()
                .echoId(program.id)
                .title(program.title)
                .announce(program.announce)
                .hosts(TextUtils.join(",", hosts))
                .pictureUrl(program.picture == null ? null : program.picture.url)
                .build());
    }

    /**
     * check if this program in favourite table.
     *
     * @param echoId program id to check
     * @return observable
     */
    public Maybe<Integer> isProgramFavourite(long echoId) {
        return mDb.createQuery(FavouriteProgram.TABLE, EXIST_QUERY, String.valueOf(echoId))
                .map(query -> {
                    Cursor cursor = query.run();
                    if (cursor == null) return 0;
                    try {
                        if (!cursor.moveToNext()) {
                            return 0;
                        } else {
                            return cursor.getInt(0);
                        }
                    } finally {
                        cursor.close();
                    }
                }).firstElement();
    }

    /**
     * Remove favourite program from db.
     *
     * @param id program.id
     */
    public int removeProgram(int id) {
        return mDb.delete(FavouriteProgram.TABLE, DELETE_WHERE, String.valueOf(id));
    }

    public Observable<List<Program>> getAllPrograms() {
        return mDb.createQuery(FavouriteProgram.TABLE, SELECT_ALL)
                .mapToList(FavouriteProgram.MAPPER);
    }
}
