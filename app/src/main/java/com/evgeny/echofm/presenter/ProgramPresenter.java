package com.evgeny.echofm.presenter;

import android.content.Context;
import android.graphics.Bitmap;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.model.Avatar;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.network.EchoApiService;

import java.util.concurrent.ExecutionException;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ProgramPresenter {
    private EchoApiService mService;

    @Inject
    public ProgramPresenter(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    public Observable<Program[]> getPrograms(int page, boolean isActive) {
        return mService.getPrograms(page, isActive, "all")
                .map(programEchoApiResponse -> programEchoApiResponse.content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Maybe<Program> getProgram(String id) {
        return mService.getProgram(id, "all")
                .map(programEchoApiResponse -> programEchoApiResponse.content[0])
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Load program avatar.
     *
     * @param context application context
     * @param id      program id
     * @return bitmap with a program avatar if the program does not have one.
     */
    public Maybe<Bitmap> getProgramPicture(Context context, int id) {
        return mService.getProgram(String.valueOf(id), "picture")
                .map(programEchoApiResponse -> {
                    Avatar picture = programEchoApiResponse.content[0].picture;
                    Bitmap bm = null;
                    if (picture != null) {
                        try {
                            bm = Glide.with(context).asBitmap()
                                    .load(picture.url)
                                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                                    .submit(picture.width, picture.height).get();
                        } catch (InterruptedException | ExecutionException e) {
                            Timber.d(e, "load program avatar failed");
                        }
                    }

                    return bm;
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }
}
