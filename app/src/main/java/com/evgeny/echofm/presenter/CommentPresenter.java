package com.evgeny.echofm.presenter;

import com.evgeny.echofm.model.Comment;
import com.evgeny.echofm.network.EchoApiService;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class CommentPresenter {

    EchoApiService mService;

    @Inject
    public CommentPresenter(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    public Maybe<Comment[]> getComment(String id) {
        return mService.comments(id, "all")
                .map(commentEchoApiResponse -> commentEchoApiResponse.content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
