package com.evgeny.echofm.presenter;

import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.network.EchoApiService;

import org.joda.time.DateTime;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


@Singleton
public class SchedulerPresenter implements ISchedulerPresenter {

    EchoApiService mService;

    @Inject
    public SchedulerPresenter(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    @Override
    public Single<ArrayList<Schedule>> getProgramsAfterNow() {
        return mService.getScheds("all")
                .flatMapObservable(response -> Observable.fromArray(response.content))
                .buffer(2, 1)
                .filter(schedules -> schedules.size() == 2)
                .filter(schedules -> new DateTime(schedules.get(1).approved_at).isAfterNow())
                .collect(() -> new ArrayList<Schedule>(), (schedules1, schedules2) -> {
                    if (schedules1.size() > 0) {
                        schedules1.remove(schedules1.size() - 1);
                    }
                    schedules1.add(schedules2.get(0));
                    schedules1.add(schedules2.get(1));
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Maybe<Schedule[]> getScheduleList() {
        return mService.getScheds("all")
                .map(scheduleEchoApiResponse -> scheduleEchoApiResponse.content)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
