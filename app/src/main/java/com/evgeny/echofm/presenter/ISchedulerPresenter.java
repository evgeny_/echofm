package com.evgeny.echofm.presenter;

import com.evgeny.echofm.model.Schedule;

import java.util.ArrayList;

import io.reactivex.Maybe;
import io.reactivex.Single;


public interface ISchedulerPresenter {
    /**
     * Get all upcoming programs for current day sorted by time.
     * The first item is a program on live now
     *
     * @return observable with single list
     */
    Single<ArrayList<Schedule>> getProgramsAfterNow();

    Maybe<Schedule[]> getScheduleList();
}
