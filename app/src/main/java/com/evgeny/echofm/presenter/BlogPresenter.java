package com.evgeny.echofm.presenter;

import com.crashlytics.android.Crashlytics;
import com.evgeny.echofm.network.EchoApiService;
import com.evgeny.echofm.network.RSSFeed;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.URL;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public class BlogPresenter {
    EchoApiService mService;

    @Inject
    public BlogPresenter(EchoApiService echoApiService) {
        mService = echoApiService;
    }

    public Observable<RSSFeed> getBlogs() {
        return mService.getBlogsXML()
                .map(rssResponse -> rssResponse.channel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Get single blog html document
     *
     * @param url blog url
     * @return html document
     */
//    public Observable<Document> getBlogDetail(final URL url) {
//        return Observable.create(sub -> {
//            sub.onNext(getPage(url));
//            sub.onCompleted();
//        });
//    }

    /**
     * Fetch web page HTML document.
     *
     * @param url web page url
     * @return html document
     */
    private Document getPage(URL url) {
        try {
            return Jsoup.parse(url, 10000);
        } catch (IOException e) {
            Crashlytics.log("fetch page " + url + " failed");
            Crashlytics.logException(e);
        }

        return null;
    }
}
