package com.evgeny.echofm.service;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaBrowserServiceCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.app.NotificationCompat.MediaStyle;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.graphics.Palette;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.FutureTarget;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.DayProgram;
import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.presenter.ProgramPresenter;
import com.evgeny.echofm.ui.media.EchoDataSourceFactory;
import com.evgeny.echofm.ui.media.PlayerActivity;
import com.evgeny.echofm.ui.schedule.OnAirActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.evgeny.echofm.player.NotificationHelper.from;

final public class PlayerService extends MediaBrowserServiceCompat {

    private final int NOTIFICATION_ID = 404;
    private final String NOTIFICATION_DEFAULT_CHANNEL_ID = "default_channel";

//    private final MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();

    private final PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder().setActions(
            PlaybackStateCompat.ACTION_PLAY
                    | PlaybackStateCompat.ACTION_STOP
                    | PlaybackStateCompat.ACTION_PAUSE
                    | PlaybackStateCompat.ACTION_PLAY_PAUSE
                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    );

    private MediaSessionCompat mediaSession;

    private AudioManager audioManager;
    private AudioFocusRequest audioFocusRequest;
    private boolean audioFocusRequested = false;

    private SimpleExoPlayer exoPlayer;
    private ExtractorsFactory extractorsFactory;
    private DataSource.Factory dataSourceFactory;

    @Inject
    public DayProgram dayProgram;

    @Inject
    ProgramPresenter programPresenter;

    @Inject
    BroadcastPresenter broadcastPresenter;

    private Bitmap programIcon;

    /**
     * id of the played broadcast
     * used only if {#isLiveSteam} is false
     */
    private long broadcastId;

//    private final MusicRepository musicRepository = new MusicRepository();

    @Override
    public void onCreate() {
        super.onCreate();

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_DEFAULT_CHANNEL_ID, "echofm", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setOnAudioFocusChangeListener(audioFocusChangeListener)
                    .setAcceptsDelayedFocusGain(false)
                    .setWillPauseWhenDucked(true)
                    .setAudioAttributes(audioAttributes)
                    .build();
        }

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mediaSession = new MediaSessionCompat(this, "PlayerService");
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(mediaSessionCallback);

        setSessionToken(mediaSession.getSessionToken());

        Context appContext = getApplicationContext();

        Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON, null, appContext, MediaButtonReceiver.class);
        mediaSession.setMediaButtonReceiver(PendingIntent.getBroadcast(appContext, 0, mediaButtonIntent, 0));

        exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(this), new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.addListener(exoPlayerListener);
        this.dataSourceFactory = new EchoDataSourceFactory();
        this.extractorsFactory = new DefaultExtractorsFactory();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // trigger a callback event
        MediaButtonReceiver.handleIntent(mediaSession, intent);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dayProgram.complete();
        mediaSession.release();
        exoPlayer.release();
    }

    @Override
    public BrowserRoot onGetRoot(@NonNull String clientPackageName, int clientUid, @Nullable Bundle rootHints) {
        return new BrowserRoot("Root", null);
    }

    @Override
    public void onLoadChildren(@NonNull String parentId, @NonNull Result<List<MediaBrowserCompat.MediaItem>> result) {
        result.detach();
    }

    private MediaSessionCompat.Callback mediaSessionCallback = new MediaSessionCompat.Callback() {

        private Uri currentUri;
        private boolean isLiveSteam;
        int currentState = PlaybackStateCompat.STATE_STOPPED;

        @Override
        public void onPlayFromMediaId(String mediaId, Bundle extras) {
            //playTrack(musicRepository.getTrackByIndex(Integer.parseInt(mediaId)));
        }

        @Override
        public void onPlayFromUri(Uri uri, Bundle extras) {
            startService(new Intent(getApplicationContext(), PlayerService.class));

            // extract information provided by activities
            extras.setClassLoader(getClassLoader());
            String streamMode = extras.getString("stream_mode");
            if (streamMode == null) throw new IllegalStateException("unknown stream mode");

            switch (streamMode) {
                case "broadcast": {
                    isLiveSteam = false;

                    // broadcast playback provide metadata bundle
                    MediaMetadataCompat mediaMetadataCompat = extras.getParcelable("media_metadata");
                    if (mediaMetadataCompat != null) {
                        mediaSession.setMetadata(mediaMetadataCompat);
                        Intent activityIntent = new Intent(getApplicationContext(), PlayerActivity.class);
                        broadcastId = extras.getLong("broadcast_id");
                        activityIntent.putExtra("broadcast_id", broadcastId);
                        activityIntent.putExtra("is_downloaded", extras.getBoolean("is_downloaded"));
                        mediaSession.setSessionActivity(PendingIntent.getActivity(getApplicationContext(), 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT));
                        String artworkUri = mediaMetadataCompat.getString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI);
                        loadArtwork(Uri.parse(artworkUri));
                    }
                    break;
                }
                case "live": {
                    // default "notification" activity
                    Intent activityIntent = new Intent(getApplicationContext(), OnAirActivity.class);
                    mediaSession.setSessionActivity(PendingIntent.getActivity(getApplicationContext(), 0, activityIntent, PendingIntent.FLAG_UPDATE_CURRENT));
                    isLiveSteam = true;
                    break;
                }
            }

            playStream(uri);
        }

        @Override
        public void onPlay() {
            startService(new Intent(getApplicationContext(), PlayerService.class));

            // resume playing stream
            if (currentUri != null) {
                playStream(currentUri);
            }
        }

        @Override
        public void onSeekTo(long pos) {
            super.onSeekTo(pos);
            exoPlayer.seekTo(pos);
            mediaSession.setPlaybackState(stateBuilder.setState(currentState, exoPlayer.getCurrentPosition(), 1).build());
        }

        /**
         * start playing stream. Async load media meta and update notification
         * @param uri - stream source
         */
        private void playStream(@NonNull Uri uri) {
            // prepare playing if is this a other eg new stream or live-stream
            if (!uri.equals(currentUri) || isLiveSteam) {
                prepareToPlay(uri);
                // start playing
                play(true);
            } else {
                play(false);
            }

            // async update notification
            if (isLiveSteam) {
                // update notification each time the program change
                dayProgram.subscribe(program -> {
                    updateMetaData(program);
                    refreshNotificationAndForegroundStatus(currentState);
                    if (program.program != null) loadProgramArt(program.program.id);
                });
            } else {
                refreshNotificationAndForegroundStatus(currentState);
            }
        }

        private void play(boolean resetPosition) {
            if (!exoPlayer.getPlayWhenReady()) {
                if (!audioFocusRequested) {
                    audioFocusRequested = true;

                    int audioFocusResult;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                    } else {
                        audioFocusResult = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                    }
                    if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                        return;
                }

                mediaSession.setActive(true);

                registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

                exoPlayer.setPlayWhenReady(true);
            }

            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING,
                    resetPosition ? 0 : exoPlayer.getCurrentPosition(), 1).build());
            currentState = PlaybackStateCompat.STATE_PLAYING;
        }

        @Override
        public void onPause() {
            // todo persist position

            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.setPlayWhenReady(false);
                unregisterReceiver(becomingNoisyReceiver);
            }

            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED, exoPlayer.getCurrentPosition(), 1).build());
            currentState = PlaybackStateCompat.STATE_PAUSED;

            refreshNotificationAndForegroundStatus(currentState);
        }

        @Override
        public void onStop() {
            // todo persist position
            if (!isLiveSteam) {
                // get current broadcast id
                //broadcastId
                // get last known playback position
                //exoPlayer.getCurrentPosition();
                // update position in broadcast repository
                broadcastPresenter.savePosition(broadcastId, exoPlayer.getCurrentPosition() > 5000 ? exoPlayer.getCurrentPosition() - 5000 : 0);
            }

            if (exoPlayer.getPlayWhenReady()) {
                exoPlayer.setPlayWhenReady(false);
                unregisterReceiver(becomingNoisyReceiver);
            }

            if (audioFocusRequested) {
                audioFocusRequested = false;

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioManager.abandonAudioFocusRequest(audioFocusRequest);
                } else {
                    audioManager.abandonAudioFocus(audioFocusChangeListener);
                }
            }

            mediaSession.setActive(false);

            mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED, exoPlayer.getCurrentPosition(), 1).build());
            currentState = PlaybackStateCompat.STATE_STOPPED;

            refreshNotificationAndForegroundStatus(currentState);

            stopSelf();
        }

//        @Override
//        public void onSkipToNext() {
//            MusicRepository.Track track = musicRepository.getNext();
//            updateMetadataFromTrack(track);
//
//            refreshNotificationAndForegroundStatus(currentState);
//
//            prepareToPlay(track.getUri());
//        }
//
//        @Override
//        public void onSkipToPrevious() {
//            MusicRepository.Track track = musicRepository.getPrevious();
//            updateMetadataFromTrack(track);
//
//            refreshNotificationAndForegroundStatus(currentState);
//
//            prepareToPlay(track.getUri());
//        }


        /**
         * init player with new stream
         * @param uri stream
         */
        private void prepareToPlay(Uri uri) {
            currentUri = uri;
            ExtractorMediaSource mediaSource = new ExtractorMediaSource(uri, dataSourceFactory, extractorsFactory, null, null);
            exoPlayer.prepare(mediaSource);
        }

        private void loadProgramArt(int programId) {
            programPresenter.getProgramPicture(getApplicationContext(), programId).subscribe(bitmap -> {
                programIcon = bitmap;
                NotificationManagerCompat.from(PlayerService.this).notify(NOTIFICATION_ID, getNotification(currentState));
            }, throwable -> Timber.w(throwable, "program icon load failed"));
        }

        // async load artwork provided by uri
        private void loadArtwork(Uri uri) {
            FutureTarget<Bitmap> task = Glide.with(PlayerService.this)
                    .asBitmap().load(uri)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)).submit();
            Maybe.fromFuture(task)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(bitmap -> {
                programIcon = bitmap;
                NotificationManagerCompat.from(PlayerService.this).notify(NOTIFICATION_ID, getNotification(currentState));
            }, throwable -> Timber.e(throwable, ""));
        }

        /**
         * Update metadata from current live program. ONLY live stream
         *
         * @param program - the program live for now
         */
        private void updateMetaData(@Nullable Schedule program) {
            String title = "";
            String album = getString(R.string.radio_name);
            String artist = "";
            if (program != null) {
                title = program.program == null ? program.title : program.program.title;
                artist = program.title;
            }

            MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, program == null ? "-1" : String.valueOf(program.id));
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, title);
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album);
            metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist);
            metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, -1);
            mediaSession.setMetadata(metadataBuilder.build());
        }
    };

    /**
     * Resume playing only when the playback was interrupted by the other application,
     * otherwise e.g. the playing was paused manually skip the autofocus handling
     */
    private boolean resumePlaying = false;
    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = focusChange -> {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                if (resumePlaying) {
                    mediaSessionCallback.onPlay();
                    resumePlaying = false;
                }
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (exoPlayer.getPlayWhenReady()) {
                    mediaSessionCallback.onPause();
                    resumePlaying = true;
                }
                break;
            default:
                mediaSessionCallback.onPause();
                break;
        }
    };

    private final BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Disconnecting headphones - stop playback
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                mediaSessionCallback.onPause();
            }
        }
    };


    private Player.EventListener exoPlayerListener = new Player.EventListener() {
        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (playWhenReady && playbackState == Player.STATE_ENDED) {
                mediaSessionCallback.onStop();
            }
        }

        @Override
        public void onRepeatModeChanged(int repeatMode) {
            // TODO
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPositionDiscontinuity() {
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (SERVICE_INTERFACE.equals(intent.getAction())) {
            return super.onBind(intent);
        }
        return new PlayerServiceBinder();
    }

    public class PlayerServiceBinder extends Binder {
        public MediaSessionCompat.Token getMediaSessionToken() {
            return mediaSession.getSessionToken();
        }
    }

    private void refreshNotificationAndForegroundStatus(int playbackState) {
        switch (playbackState) {
            case PlaybackStateCompat.STATE_PLAYING: {
                startForeground(NOTIFICATION_ID, getNotification(playbackState));
                break;
            }
            case PlaybackStateCompat.STATE_PAUSED: {
                NotificationManagerCompat.from(PlayerService.this).notify(NOTIFICATION_ID, getNotification(playbackState));
//                stopForeground(false);
                break;
            }
            case PlaybackStateCompat.STATE_STOPPED: {
                stopForeground(true);
                break;
            }
            default: {
                stopForeground(true);
                break;
            }
        }
    }

    /**
     * @param playbackState state
     * @return notification
     */
    private Notification getNotification(int playbackState) {
        NotificationCompat.Builder builder = from(this, mediaSession);
        //builder.addAction(new NotificationCompat.Action(android.R.drawable.ic_media_previous, getString(R.string.previous), MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS)));

        if (playbackState == PlaybackStateCompat.STATE_PLAYING)
            builder.addAction(new NotificationCompat.Action(R.drawable.ic_pause_white_36dp, getString(R.string.notofication_action_pause),
                    MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));
        else
            builder.addAction(new NotificationCompat.Action(R.drawable.ic_play_arrow_white_36dp, getString(R.string.notofication_action_play),
                    MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY_PAUSE)));

        builder.addAction(R.drawable.ic_clear_white_36dp, getString(R.string.notofication_action_cancel), MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_STOP));

        //builder.addAction(new NotificationCompat.Action(android.R.drawable.ic_media_next, getString(R.string.next), MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_SKIP_TO_NEXT)));
        builder.setStyle(new MediaStyle()
                .setShowActionsInCompactView(0)
                .setShowCancelButton(true)
                .setCancelButtonIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_STOP))
                .setMediaSession(mediaSession.getSessionToken()));
        builder.setSmallIcon(R.drawable.ic_mic_selector);
        builder.setShowWhen(false);
        builder.setPriority(NotificationCompat.PRIORITY_HIGH);
        builder.setChannelId(NOTIFICATION_DEFAULT_CHANNEL_ID);
        builder.setOnlyAlertOnce(true);
        if (programIcon != null) {
            builder.setLargeIcon(programIcon);
            builder.setColor(Palette.from(programIcon).generate().getVibrantColor(Color.parseColor("#403f4d")));
        }


        return builder.build();
    }
}
