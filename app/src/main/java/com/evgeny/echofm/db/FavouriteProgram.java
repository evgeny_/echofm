package com.evgeny.echofm.db;


import android.content.ContentValues;
import android.database.Cursor;

import com.evgeny.echofm.model.Avatar;
import com.evgeny.echofm.model.Program;
import com.google.auto.value.AutoValue;

import io.reactivex.functions.Function;

@AutoValue
public abstract class FavouriteProgram {
    public static final String TABLE = "favourite_program";

    public static final String ID = "_id";
    public static final String ECHO_ID = "echo_id";
    public static final String TITLE = "title";
    public static final String ANNOUNCE = "announce";
    public static final String HOSTS = "hosts";
    public static final String PICTURE_URL = "picture_url";

    public abstract long id();

    public abstract long echoId();

    public abstract String title();

    public abstract String announce();

    public abstract String hosts();

    public abstract String pictureUrl();

/*    public static final Func1<Cursor, FavouriteProgram> MAPPER = new Func1<Cursor, FavouriteProgram>() {
        @Override public FavouriteProgram call(Cursor cursor) {
            long id = Db.getLong(cursor, ID);
            long echoId = Db.getLong(cursor, ECHO_ID);
            String title = Db.getString(cursor, TITLE);
            String announce = Db.getString(cursor, ANNOUNCE);
            String pictureUrl = Db.getString(cursor, PICTURE_URL);
            return new AutoValue_FavouriteProgram(id, echoId, title, announce, pictureUrl);
        }
    };*/


    public static final Function<Cursor, Program> MAPPER = new Function<Cursor, Program>() {
        @Override
        public Program apply(Cursor cursor) {
            Program program = new Program();

            program.id = Db.getInt(cursor, ECHO_ID);
            program.title = Db.getString(cursor, TITLE);
            program.announce = Db.getString(cursor, ANNOUNCE);
            // TODO set hosts
            //program.getHosts()

            Avatar avatar = new Avatar();
            avatar.url = Db.getString(cursor, PICTURE_URL);

            program.picture = avatar;
            return program;
        }
    };

    public static final class Builder {
        private final ContentValues values = new ContentValues();

        public Builder id(long id) {
            values.put(ID, id);
            return this;
        }

        public Builder echoId(long echoId) {
            values.put(ECHO_ID, echoId);
            return this;
        }


        public Builder title(String title) {
            values.put(TITLE, title);
            return this;
        }

        public Builder announce(String announce) {
            values.put(ANNOUNCE, announce);
            return this;
        }

        public Builder hosts(String hosts) {
            values.put(HOSTS, hosts);
            return this;
        }

        public Builder pictureUrl(String pictureUrl) {
            values.put(PICTURE_URL, pictureUrl);
            return this;
        }


        public ContentValues build() {
            return values;
        }
    }
}
