package com.evgeny.echofm.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.db.SupportSQLiteOpenHelper;


public class DbCallback extends SupportSQLiteOpenHelper.Callback {

    private static final int VERSION = 3;

    private static final String CREATE_FAVOURITE = ""
            + "CREATE TABLE " + FavouriteProgram.TABLE + "("
            + FavouriteProgram.ID + " INTEGER NOT NULL PRIMARY KEY,"
            + FavouriteProgram.ECHO_ID + " INTEGER UNIQUE,"
            + FavouriteProgram.TITLE + " TEXT,"
            + FavouriteProgram.ANNOUNCE + " TEXT,"
            + FavouriteProgram.HOSTS + " TEXT,"
            + FavouriteProgram.PICTURE_URL + " TEXT"
            + ");";

    private static final String CREATE_DOWNLOADED = ""
            + "CREATE TABLE " + DownloadedBroadcast.TABLE + "("
            + DownloadedBroadcast.ECHO_ID + " INTEGER NOT NULL UNIQUE,"
            + DownloadedBroadcast.FILE_PATH + " TEXT,"
            + DownloadedBroadcast.TIME_PLAYED + " INTEGER,"
            + DownloadedBroadcast.IS_NEW_RECORD + " INTEGER,"
            + DownloadedBroadcast.BROADCAST + " BLOB"
            + ");";

    private static final String DROP_DOWNLOADED = "DROP TABLE " + DownloadedBroadcast.TABLE;

    /**
     * Creates a new Callback to get database lifecycle events.
     */
    public DbCallback() {
        super(VERSION);
    }

    @Override
    public void onCreate(SupportSQLiteDatabase db) {
        db.execSQL(CREATE_FAVOURITE);
        db.execSQL(CREATE_DOWNLOADED);
    }

    @Override
    public void onUpgrade(SupportSQLiteDatabase db, int oldVersion, int newVersion) {
        //update db schema here if needed
        if (oldVersion <= 2) {
            db.execSQL(DROP_DOWNLOADED);
            db.execSQL(CREATE_DOWNLOADED);
        }
    }
}
