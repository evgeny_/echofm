package com.evgeny.echofm.db;

import android.content.ContentValues;
import android.database.Cursor;

import io.reactivex.functions.Function;


public class DownloadedBroadcast {
    public static final String TABLE = "downloaded_bcast";

    public static final String ECHO_ID = "echo_id";
    public static final String FILE_PATH = "file_path";
    public static final String TIME_PLAYED = "time_played";
    public static final String IS_NEW_RECORD = "is_new_record";
    public static final String BROADCAST = "broadcast";

    private final long echoId;
    private String filePath;
    private boolean isNewRecord;
    private long timePlayed;
    private byte[] broadcast;

    private DownloadedBroadcast(Builder builder) {
        echoId = builder.echoId;
        filePath = builder.filePath;
        isNewRecord = builder.isNewRecord;
        timePlayed = builder.timePlayed;
        broadcast = builder.broadcast;
    }

    public long echoId() {
        return echoId;
    }

    public String filePath() {
        return filePath;
    }

    public boolean isNewRecord() {
        return isNewRecord;
    }

    public long timePlayed() {
        return timePlayed;
    }

    public byte[] broadcast() {
        return broadcast;
    }

    public synchronized void updateFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static class Builder {

        private long echoId;
        private String filePath;
        private boolean isNewRecord;
        private long timePlayed;
        private byte[] broadcast;

        public Builder(long echoId) {
            this.echoId = echoId;
        }

        public Builder filePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public Builder isNewRecord(boolean isNewRecord) {
            this.isNewRecord = isNewRecord;
            return this;
        }

        public Builder timePlayed(long timePlayed) {
            this.timePlayed = timePlayed;
            return this;
        }

        public Builder broadcast(byte[] broadcast) {
            this.broadcast = broadcast;
            return this;
        }

        public DownloadedBroadcast build() {
            return new DownloadedBroadcast(this);
        }
    }

    public static final class ContentValueBuilder {
        private final ContentValues values = new ContentValues();

        public ContentValueBuilder echoId(long echoId) {
            values.put(ECHO_ID, echoId);
            return this;
        }

        public ContentValueBuilder filePath(String filePath) {
            values.put(FILE_PATH, filePath);
            return this;
        }

        public ContentValueBuilder timePlayed(long timePlayed) {
            values.put(TIME_PLAYED, timePlayed);
            return this;
        }

        public ContentValueBuilder newRecord(int isNewRecord) {
            values.put(IS_NEW_RECORD, isNewRecord);
            return this;
        }

        public ContentValueBuilder broadcast(byte[] broadcast) {
            values.put(BROADCAST, broadcast);
            return this;
        }


        public ContentValues build() {
            return values;
        }
    }

    public static final Function<Cursor, DownloadedBroadcast> MAPPER = new Function<Cursor, DownloadedBroadcast>() {
        @Override
        public DownloadedBroadcast apply(Cursor cursor) {
            long echoId = Db.getLong(cursor, ECHO_ID);
            String filePath = Db.getString(cursor, FILE_PATH);
            boolean newRecord = Db.getBoolean(cursor, IS_NEW_RECORD);
            long timePlayed = Db.getLong(cursor, TIME_PLAYED);
            byte[] bcast = Db.getByteArray(cursor, BROADCAST);

            return new Builder(echoId)
                    .filePath(filePath)
                    .isNewRecord(newRecord)
                    .timePlayed(timePlayed)
                    .broadcast(bcast)
                    .build();
        }
    };
}
