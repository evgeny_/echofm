package com.evgeny.echofm.db;

import android.content.ContentValues;

import java.util.List;

import io.reactivex.Maybe;

public interface IBroadcastRepository {
    Maybe<List<DownloadedBroadcast>> getAll();

    Maybe<DownloadedBroadcast> get(long echoId);

    void removeAll();

    boolean remove(long echoId);

    long add(DownloadedBroadcast broadcast);

    void update(long echoId, ContentValues values);
}
