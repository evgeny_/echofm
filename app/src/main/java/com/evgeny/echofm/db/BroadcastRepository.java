package com.evgeny.echofm.db;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite3.BriteDatabase;

import java.util.List;

import io.reactivex.Maybe;

public class BroadcastRepository implements IBroadcastRepository {
    private static final String SELECT_ALL = "SELECT * FROM " + DownloadedBroadcast.TABLE + " ORDER BY rowid DESC";
    private static final String SELECT_BY_ECHO_ID = "SELECT * FROM " + DownloadedBroadcast.TABLE
            + " WHERE " + DownloadedBroadcast.ECHO_ID + " = ? LIMIT 1";

    private final BriteDatabase mDb;

    public BroadcastRepository(BriteDatabase mDb) {
        this.mDb = mDb;
    }

    @Override
    public Maybe<List<DownloadedBroadcast>> getAll() {
        return mDb.createQuery(DownloadedBroadcast.TABLE, SELECT_ALL)
                .mapToList(DownloadedBroadcast.MAPPER).firstElement();
    }

    @Override
    public Maybe<DownloadedBroadcast> get(long echoId) {
        return mDb.createQuery(DownloadedBroadcast.TABLE, SELECT_BY_ECHO_ID, echoId)
                .mapToOneOrDefault(DownloadedBroadcast.MAPPER, new DownloadedBroadcast.Builder(-1).build())
                .firstElement();
    }

    @Override
    public void removeAll() {
        mDb.delete(DownloadedBroadcast.TABLE, null);
    }

    @Override
    public boolean remove(long echoId) {
        return mDb.delete(DownloadedBroadcast.TABLE, DownloadedBroadcast.ECHO_ID + "= ?", String.valueOf(echoId)) == 1;
    }

    @Override
    public long add(DownloadedBroadcast broadcast) {
        return mDb.insert(DownloadedBroadcast.TABLE, SQLiteDatabase.CONFLICT_IGNORE,
                new DownloadedBroadcast.ContentValueBuilder()
                        .echoId(broadcast.echoId())
                        .filePath(broadcast.filePath())
                        .timePlayed(broadcast.timePlayed())
                        .newRecord(broadcast.isNewRecord() ? 1 : 0)
                        .broadcast(broadcast.broadcast())
                        .build());
    }

    @Override
    public void update(long echoId, ContentValues values) {
        mDb.update(DownloadedBroadcast.TABLE, SQLiteDatabase.CONFLICT_IGNORE,
                values, DownloadedBroadcast.ECHO_ID + "=?", String.valueOf(echoId));
    }
}
