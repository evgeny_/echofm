package com.evgeny.echofm.ui.blog;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BlogPresenter;
import com.evgeny.echofm.ui.BaseFragment;

import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class BlogListFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView mRecycleView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private Unbinder unbinder;

    private RSSFeedAdapter mAdapter;

    @Inject
    BlogPresenter mBlogPresenter;

    public BlogListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_blog_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this::getBlogRSS);

        mRecycleView.setHasFixedSize(true);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));

        mAdapter = new RSSFeedAdapter(Collections.emptyList());

        // Set NewsAdapter as the adapter for RecyclerView.
        mRecycleView.setAdapter(mAdapter);

        /** request news */
        getBlogRSS();

        return view;
    }

    /**
     * Retrieve actual blog rss.
     */
    private void getBlogRSS() {
        mSwipeRefreshLayout.setRefreshing(true);

        mBlogPresenter.getBlogs()
                .compose(this.bindToLifecycle())
                .subscribe(rssFeed -> {
                    mAdapter.setData(rssFeed.entries);
                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> {
                    Timber.w(throwable, "blogs rss request failed");
                    if (isVisible()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        Snackbar.make(getView(), R.string.no_connection, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }
}
