package com.evgeny.echofm.ui.media;

import com.google.android.exoplayer2.upstream.DataSource;


public class EchoDataSourceFactory implements DataSource.Factory {
    @Override
    public DataSource createDataSource() {
        return new EchoDataSource();
    }
}
