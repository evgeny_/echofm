package com.evgeny.echofm.ui.blog;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BlogDetailPresenter;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.utils.DrawableUtils;
import com.evgeny.echofm.utils.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BlogDetailActivity extends BaseActivity {

    @BindView(R.id.web_view)
    WebView mWebView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout mToolbarLayout;

    @Inject
    BlogDetailPresenter mBlogPresenter;

    /**
     * blog url
     */
    private Uri mUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog_detail);

        ButterKnife.bind(this);
        ((EchoApplication) getApplication()).plusBlogComponent().inject(this);

        mWebView.getSettings().setLoadWithOverviewMode(true);
        mWebView.getSettings().setJavaScriptEnabled(false);
        mWebView.getSettings().setAllowFileAccess(false);

        mUri = getIntent().getData();

        URL url = null;
        try {
            url = new URL(mUri.getScheme(), mUri.getHost(), mUri.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (url != null) {
            Disposable subscribe = Maybe.fromCallable(fetchBlog(url))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::parsePage, throwable -> {
                            }
                    );
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private Callable<Document> fetchBlog(URL url) {
        return () -> Jsoup.parse(url, 3000);
    }

    /**
     * Get content from html document and bind it to layout.
     *
     * @param doc html document
     */
    private void parsePage(Document doc) {
        // get content section
        Element content = doc.select("section.content").first();

        // get title
        Element title = content.select(".conthead.blog h1").first();
        mToolbarLayout.setTitle(title.text());

        Elements links = content.select("a");
        links.attr("style", "color:#ed1c24;");
        Element text = content.select("div.typical.include-relap-widget").first();
        boolean isNightTheme = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.theme_switch), false);
        if (isNightTheme) {
            text.attr("style", "color:white;");
        } else {
            text.attr("style", "color:black;");
        }

        String html = text.outerHtml();

        mWebView.setBackgroundColor(Color.TRANSPARENT);
        mWebView.loadDataWithBaseURL("http://echo.msk.ru", createPageStyle() + html, "text/html", "UTF-8", null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_blog_detail, menu);
        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_share:
                TextUtils.shareText(this, mToolbarLayout.getTitle() + " " + mUri);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        ((EchoApplication) getApplication()).clearBlogComponent();
    }

    private String createPageStyle() {
        StringBuilder styleBuilder = new StringBuilder("<style>");
        styleBuilder.append("img{display: inline;height: auto;max-width: 100%;}");

        // background color, according app theme
//        boolean isNightTheme = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.theme_switch), false);
//        if (isNightTheme) {
//            styleBuilder.append("article{color:white;}");
//        } else {
//            styleBuilder.append("article{color:black;}");
//        }

        styleBuilder.append("</style>");

        return styleBuilder.toString();
    }
}
