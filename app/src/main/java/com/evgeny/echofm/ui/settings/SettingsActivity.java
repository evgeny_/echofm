package com.evgeny.echofm.ui.settings;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.v4.app.NavUtils;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.Toast;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.ui.MainActivity;
import com.evgeny.echofm.utils.FileUtils;

import javax.inject.Inject;

/**
 *
 */
public class SettingsActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActionBar();

        getFragmentManager().beginTransaction().add(android.R.id.content, new GeneralPreferenceFragment()).commit();
    }

    /**
     * Set up the {@link android.app.ActionBar}, if the API is available.
     */
    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (!super.onMenuItemSelected(featureId, item)) {
                NavUtils.navigateUpFromSameTask(this);
            }
            return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    public static class GeneralPreferenceFragment extends PreferenceFragment {

        @Inject
        BroadcastPresenter mBroadcastPresenter;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);
            addPreferencesFromResource(R.xml.pref_general);
//            setHasOptionsMenu(true);

            findPreference(getString(R.string.direct_share)).setOnPreferenceClickListener(preference -> {
                shareApp();
                return true;
            });
            findPreference(getString(R.string.rate_app)).setOnPreferenceClickListener(preference -> {
                rateApp();
                return true;
            });
            findPreference(getString(R.string.send_feedback)).setOnPreferenceClickListener(preference -> {
                sendFeedback();
                return true;
            });

            findPreference(getString(R.string.remove_records)).setOnPreferenceClickListener(preference -> {
                removeRecords();
                return true;
            });

            findPreference(getString(R.string.theme_switch)).setOnPreferenceChangeListener((preference, newValue) -> {
                Intent restartAppIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                restartAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(restartAppIntent);
                return true;
            });

            findPreference(getString(R.string.font_size)).setOnPreferenceChangeListener((preference, newValue) -> {
                Intent restartAppIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                restartAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(restartAppIntent);
                return true;
            });
        }

        private void removeRecords() {
            mBroadcastPresenter.removeAllBroadcasts();
            FileUtils.cleanEchoDirectory();
            Intent restartAppIntent = new Intent(getActivity().getApplicationContext(), MainActivity.class);
            restartAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(restartAppIntent);
            Toast.makeText(getActivity(), R.string.setting_remove_records_toast, Toast.LENGTH_SHORT).show();
        }

        /**
         * Share app link with user friends.
         */
        private void shareApp() {
            Intent sharingIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setType("text/plain")
                    .setText(getString(R.string.setting_share_app_message,
                            "http://play.google.com/store/apps/details?id=" + getActivity().getPackageName()))
                    .getIntent();

            startActivity(Intent.createChooser(sharingIntent, getString(R.string.setting_share_app_intent_title)));
        }

        /**
         * Open app page in the market app or in internet browser, to late user rate this app.
         */
        private void rateApp() {
            Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
            Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
//            // To count with Play market backstack, After pressing back button,
//            // to taken back to our application, we need to add following flags to intent.
//            goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
//                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
//                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            try {
                startActivity(goToMarket);
            } catch (ActivityNotFoundException e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
            }
        }

        /**
         * send feedback per email
         */
        private void sendFeedback() {
            Intent emailIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setType("text/email")
                    .setSubject(getString(R.string.feedback_subject))
                    .addEmailTo(getString(R.string.feedback_email))
                    .getIntent();

            startActivity(Intent.createChooser(emailIntent, getString(R.string.setting_feedback_title)));
        }
    }
}
