package com.evgeny.echofm.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.evgeny.echofm.R;
import com.evgeny.echofm.ui.blog.BlogListFragment;
import com.evgeny.echofm.ui.broadcast.BroadcastDownloadedActivity;
import com.evgeny.echofm.ui.broadcast.BroadcastListFragment;
import com.evgeny.echofm.ui.news.NewsListFragment;
import com.evgeny.echofm.ui.program.FavouriteProgramListActivity;
import com.evgeny.echofm.ui.program.ProgramListActivity;
import com.evgeny.echofm.ui.schedule.OnAirActivity;
import com.evgeny.echofm.ui.schedule.ScheduleActivity;
import com.evgeny.echofm.ui.settings.AboutActivity;
import com.evgeny.echofm.ui.settings.SettingsActivity;
import com.evgeny.echofm.utils.DrawableUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.nav_view)
    NavigationView mNavigationView;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // set theme eg night or day mode
        boolean isNightTheme = PreferenceManager.getDefaultSharedPreferences(this).getBoolean(getString(R.string.theme_switch), false);
        if (isNightTheme) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        mDrawerToggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(this);

        mViewPager.setOffscreenPageLimit(2);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new NewsListFragment(), getString(R.string.tab_news));
        adapter.addFrag(new BroadcastListFragment(), getString(R.string.tab_bcasts));
        adapter.addFrag(new BlogListFragment(), getString(R.string.tab_blogs));
        mViewPager.setAdapter(adapter);

        mTabLayout.setupWithViewPager(mViewPager);

        showUpdateDialog();
    }

    /**
     * show this dialog this update information, only in case user update the app
     */
    private void showUpdateDialog() {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String newFeatureDesc = getString(R.string.new_features_desc);
        int newFeatureDescHash = defaultSharedPreferences.getInt("update_msg_hash", 0);
        if (newFeatureDesc.hashCode() != newFeatureDescHash) {
            // show new feature dialog because desc text was changed
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.new_features_title));
            builder.setMessage(R.string.new_features_desc);
            builder.setPositiveButton(android.R.string.ok, (dialogInterface, i) -> {
                dialogInterface.dismiss();
                requestPermissions();
            });
            builder.show();

            // persist desc message hash code to skip dialog showing on next app start
            defaultSharedPreferences.edit().putInt("update_msg_hash", newFeatureDesc.hashCode()).apply();
        } else {
            requestPermissions();
        }
    }

    @Override
    protected void onDestroy() {
        mDrawerLayout.removeDrawerListener(mDrawerToggle);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        // set tint according current theme(day or night)
        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_bcast) {
            Intent intent = new Intent(getApplicationContext(), OnAirActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_programs) {
            startActivity(new Intent(this, ProgramListActivity.class));
        } else if (id == R.id.nav_scheds) {
            startActivity(new Intent(this, ScheduleActivity.class));
        } else if (id == R.id.nav_favorites) {
            startActivity(new Intent(this, FavouriteProgramListActivity.class));
        } else if (id == R.id.nav_downloaded) {
            startActivity(new Intent(this, BroadcastDownloadedActivity.class));
        } else if (id == R.id.nav_about) {
            startActivity(new Intent(this, AboutActivity.class));
        } else if (id == R.id.nav_setting) {
            startActivity(new Intent(this, SettingsActivity.class));
        }

        mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void requestPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
//                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//
//                Toast.makeText(this, "hopla explanation", Toast.LENGTH_SHORT).show();
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        0);
//            }
        }
    }

    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
