package com.evgeny.echofm.ui.program;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.presenter.FavouriteProgramPresenter;
import com.evgeny.echofm.presenter.ProgramPresenter;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.utils.DrawableUtils;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class ProgramDetailActivity extends BaseActivity {

    @Inject
    ProgramPresenter mProgramPresenter;

    @Inject
    FavouriteProgramPresenter mFavouriteProgramPresenter;

    @BindView(R.id.root_view)
    View mRootView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout toolbarLayout;

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    /**
     *
     */
    Program mProgram;

    /**
     * is this program are favourite
     */
    boolean isFavourite;

    protected Maybe<Program> mProgramObservable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_detail);

        ButterKnife.bind(this);
        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        int id = getIntent().getIntExtra("id", -1);
        if (id == -1) {
            Timber.i("invalid program id");
            // TODO show error view
            return;
        }

        mProgramObservable = mProgramPresenter.getProgram(String.valueOf(id))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .cache()
                .doOnSuccess(this::bindData);

        mProgramObservable.flatMap(program -> mFavouriteProgramPresenter.isProgramFavourite(program.id))
                .compose(this.bindToLifecycle())
                .subscribe(rowNumb -> {
                    isFavourite = rowNumb > 0;
                    invalidateOptionsMenu();
                }, throwable -> {
                    Timber.e(throwable, "program fetch error id=%d", id);
                });

        /* view pager */
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ProgramDetailFragment(), getString(R.string.program_tab_about));
        adapter.addFragment(ProgramArchiveFragment.newInstance(id), getString(R.string.program_tab_archive));
        mViewPager.setAdapter(adapter);

        mTabLayout.setupWithViewPager(mViewPager);
    }

    /**
     * @param program program to display
     */
    private void bindData(Program program) {
        mProgram = program;
        Timber.d("program name to display=%s", program.title);

        if (program.picture != null && program.picture.url != null) {

            Glide.with(this)
                    .load(program.picture.url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imageView);
        }
        toolbarLayout.setTitle(program.title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_program_details, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_save_to_favourite);
        if (isFavourite) {
            menuItem.setIcon(R.drawable.ic_favorite);
        } else {
            menuItem.setIcon(R.drawable.ic_favorite_border);
        }

        // set tint according current theme(day or night)
        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save_to_favourite && mProgram != null) {
            int snackbarMsgId;
            if (isFavourite) {
                snackbarMsgId = R.string.program_snackbar_favourite_removed;
                mFavouriteProgramPresenter.removeProgram(mProgram.id);
                isFavourite = false;
            } else {
                snackbarMsgId = R.string.program_snackbar_favourite_added;
                mFavouriteProgramPresenter.insertProgram(mProgram);
                isFavourite = true;
            }

            invalidateOptionsMenu();
            Snackbar.make(mRootView, snackbarMsgId, Snackbar.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//
//        ((EchoApplication) getApplication()).clearDatabaseComponent();
//    }

    /**
     * View pager to present comments and info fragments
     */
    public static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
