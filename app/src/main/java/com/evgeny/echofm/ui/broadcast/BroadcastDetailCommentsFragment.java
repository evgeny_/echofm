package com.evgeny.echofm.ui.broadcast;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Comment;
import com.evgeny.echofm.presenter.CommentPresenter;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.ui.BaseFragment;
import com.evgeny.echofm.ui.comment.CommentAdapter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Maybe;

public class BroadcastDetailCommentsFragment extends BaseFragment {

    @BindView(R.id.list_comments)
    RecyclerView commentList;

    @BindView(R.id.empty)
    TextView emptyView;

    @Inject
    CommentPresenter mCommentPresenter;

    private CommentAdapter adapter;
    private Unbinder unbinder;

    public BroadcastDetailCommentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_broadcast_detail_comments, container, false);
        unbinder = ButterKnife.bind(this, view);

        ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);

        commentList.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        commentList.setLayoutManager(layoutManager);
        adapter = new CommentAdapter(new Comment[0]);
        commentList.setAdapter(adapter);

        if (getActivity() instanceof BroadcastDetailActivity) {
            Maybe<Broadcast> observable = ((BroadcastDetailActivity) getActivity()).getBroadcastObservable();
            observable.flatMap(broadcast -> mCommentPresenter.getComment(String.valueOf(broadcast.id)))
                    .compose(this.bindToLifecycle())
                    .subscribe(comments -> {
                        if (comments.length == 0) {
                            emptyView.setVisibility(View.VISIBLE);
                        } else {
                            adapter.setData(comments);
                        }
                    }, throwable -> emptyView.setVisibility(View.VISIBLE));
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }
}
