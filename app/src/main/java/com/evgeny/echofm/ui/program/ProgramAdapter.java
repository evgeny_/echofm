package com.evgeny.echofm.ui.program;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Program;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.functions.Consumer;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
class ProgramAdapter extends RecyclerView.Adapter<ProgramAdapter.ViewHolder> implements Consumer<List<Program>> {
    private List<Program> mDataSet;
    private View.OnClickListener mListener;

    public void setData(List<Program> data) {
        mDataSet = data;
        notifyDataSetChanged();
    }

    void addData(List<Program> data) {
        mDataSet.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void accept(List<Program> programs) throws Exception {
        setData(programs);
    }

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView titleView;
        @BindView(R.id.description)
        TextView descriptionView;
        @BindView(R.id.picture)
        ImageView pictureView;
        @BindView(R.id.hosts)
        TextView hostsView;

        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);
        }

        public void setTitle(String title) {
            titleView.setText(title);
        }

        public void setDescription(String description) {
            descriptionView.setText(description);
        }

        /**
         * Save program position to get it later in onclicklistener
         *
         * @param id item(program) position in adapter
         */
        public void setId(int id) {
            itemView.setTag(id);
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    ProgramAdapter(List<Program> dataSet, View.OnClickListener listener) {
        mDataSet = dataSet;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_program_list, viewGroup, false);
        v.setOnClickListener(mListener);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Program program = mDataSet.get(position);

        viewHolder.setId(position);
        viewHolder.setTitle(program.title);
        viewHolder.setDescription(program.announce);

        if (program.getHosts().length == 0) {
            viewHolder.hostsView.setVisibility(View.GONE);
        } else {
            String hosts = Arrays.toString(program.getHosts());
            String hostsLine = viewHolder.itemView.getContext().getString(R.string.program_hosts, hosts.substring(1, hosts.length() - 1));
            viewHolder.hostsView.setText(hostsLine);
            viewHolder.hostsView.setVisibility(View.VISIBLE);
        }

        if (program.picture != null) {
            Glide.with(viewHolder.pictureView.getContext())
                    .load(program.picture.url).apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(viewHolder.pictureView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    Program getItem(int position) {
        return mDataSet.get(position);
    }

}
