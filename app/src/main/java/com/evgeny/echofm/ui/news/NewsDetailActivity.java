package com.evgeny.echofm.ui.news;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.News;
import com.evgeny.echofm.presenter.CommentPresenter;
import com.evgeny.echofm.presenter.NewsPresenter;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.ui.comment.CommentAdapter;
import com.evgeny.echofm.utils.DrawableUtils;
import com.evgeny.echofm.utils.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class NewsDetailActivity extends BaseActivity {

    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

    @BindView(R.id.title)
    TextView titleView;

    @BindView(R.id.news_body)
    TextView newsBodyView;

    @BindView(R.id.approved_at_date)
    TextView approvedDateView;

    @BindView(R.id.approved_at_time)
    TextView approvedTimeView;

    @BindView(R.id.list_comments)
    RecyclerView commentList;

    @BindView(R.id.enable_comments_button)
    Button commentButton;

    @BindView(R.id.comment_load_indicator)
    ProgressBar commentLoadIndicator;

    @Inject
    NewsPresenter mNewsPresenter;

    @Inject
    CommentPresenter mCommentPresenter;

    @Inject
    SharedPreferences mSharedPreference;

    /**
     * news entity
     */
    private News mNews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

//        getDelegate().setHandleNativeActionModesEnabled(true);

        // set toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        commentList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        commentList.setNestedScrollingEnabled(false);

        Integer id = getIntent().getIntExtra("id", -1);
        if (id == -1) {
            // TODO
        }

        showOverlay();

        mNewsPresenter.getNewsDetail(String.valueOf(id))
                .compose(this.bindToLifecycle())
                .subscribe(news -> {
                    hideOverlay();
                    mNews = news;
                    newsBodyView.setText(Html.fromHtml(news.body));
                    approvedDateView.setText(dateFormatter.format(news.approved_at));
                    approvedTimeView.setText(timeFormatter.format(news.approved_at));
                    titleView.setText(news.title);
                }, throwable -> {
                    showOverlay(R.string.overlay_message_error, R.drawable.ic_image_broken);
                    Timber.w(throwable, "getNewsDetail(%d):failed", id);
                });

        // load news comments
        if (!mSharedPreference.getBoolean(getString(R.string.show_news_comments_switch), false)) {
            loadComments(id);
        } else {
            commentButton.setOnClickListener(view -> {
                view.setVisibility(View.GONE);
                loadComments(id);
            });
            commentButton.setVisibility(View.VISIBLE);
        }

        newsBodyView.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                mode.setTitle(null);
                final MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_news_detail, menu);

                DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(NewsDetailActivity.this, R.color.colorPrimaryIcon));
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                // Remove the "select all" option
                menu.removeItem(android.R.id.selectAll);
                // Remove the "cut" option
                menu.removeItem(android.R.id.cut);
                // Remove the "copy all" option
                menu.removeItem(android.R.id.copy);
                return true;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_share:
                        CharSequence selectedText = TextUtils.getSelectedText(newsBodyView);
                        TextUtils.shareText(NewsDetailActivity.this, selectedText.toString());
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode mode) {

            }
        });
    }

    /**
     * load and present comments to ui
     * @param id news id
     */
    private void loadComments(Integer id) {
        commentLoadIndicator.setVisibility(View.VISIBLE);
        mCommentPresenter.getComment(String.valueOf(id))
                .subscribe(comments -> {
                            commentLoadIndicator.setVisibility(View.GONE);
                            if (comments.length > 0) {
                                commentList.setAdapter(new CommentAdapter(comments));
                            }
                        },
                        throwable -> Timber.w(throwable, "getComment(%d)", id));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news_detail, menu);

        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_share:
                if (mNews != null)
                    TextUtils.shareText(this, mNews.title + " " + "http://echo.msk.ru" + mNews.url);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
