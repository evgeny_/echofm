package com.evgeny.echofm.ui.comment;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Comment;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private Comment[] comments;

    public CommentAdapter(Comment[] comments) {
        this.comments = comments;
    }

    /**
     * Set new data to adapter
     *
     * @param comments new comments array
     */
    public void setData(Comment[] comments) {
        this.comments = comments;
        notifyDataSetChanged();
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CommentAdapter.ViewHolder holder, int position) {
        Comment c = comments[position];

        try {
            holder.getCommentView().setText(Html.fromHtml(c.body));
            holder.getUserName().setText(Html.fromHtml(c.created_by.name));

            String url = c.created_by.avatar.url;
            Glide.with(holder.getAvatar().getContext())
                    .load(url)
                    .into(holder.getAvatar());
        } catch (NullPointerException e) {
            // avatar is not available
        }
    }

    @Override
    public int getItemCount() {
        return comments.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView message;
        private final ImageView avatar;
        private final TextView userName;

        public ViewHolder(View itemView) {
            super(itemView);

            message = (TextView) itemView.findViewById(R.id.text);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            userName = (TextView) itemView.findViewById(R.id.user_name);
        }

        public TextView getCommentView() {
            return message;
        }

        public TextView getUserName() {
            return userName;
        }

        public ImageView getAvatar() {
            return avatar;
        }
    }
}
