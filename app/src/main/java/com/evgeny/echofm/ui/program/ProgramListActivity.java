package com.evgeny.echofm.ui.program;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.presenter.FavouriteProgramPresenter;
import com.evgeny.echofm.presenter.ProgramPresenter;
import com.evgeny.echofm.ui.BaseActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ProgramListActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecycleView;

    @BindView(R.id.overlay)
    View mOverlayView;

    private ProgramAdapter mAdapter;

    private final static String ACTION_SELECT = "action_select";

    @Inject
    FavouriteProgramPresenter mFavouriteProgramPresenter;

    /**
     * next page to load
     */
    private int mNextPage = 0;

    private RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            if (newState == RecyclerView.SCROLL_STATE_IDLE && !ViewCompat.canScrollVertically(recyclerView, 1)) {
                mRecycleView.removeOnScrollListener(this);
                addProgram();
            }
        }
    };

    @Inject
    ProgramPresenter mProgramPresenter;

    /**
     * @param context context
     */
    public static void startForSelect(Activity context) {
        Intent intent = new Intent(context, ProgramListActivity.class);
        intent.setAction(ACTION_SELECT);
        context.startActivityForResult(intent, 23123);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_list);

        ButterKnife.bind(this);
        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        // get action
        String action = getIntent().getAction();

        // set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecycleView.setHasFixedSize(true);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));
        mRecycleView.addOnScrollListener(onScrollListener);

        View.OnClickListener onClickListener;
        if (TextUtils.equals(action, ACTION_SELECT)) {
            onClickListener = view -> {
                Program program = mAdapter.getItem((int) view.getTag());
                mFavouriteProgramPresenter.insertProgram(program);
                finish();
            };
        } else {
            onClickListener = view -> {
                Program program = mAdapter.getItem((int) view.getTag());
                Intent i = new Intent(ProgramListActivity.this, ProgramDetailActivity.class);
                i.putExtra("id", program.id);
                startActivity(i);
            };
        }

        mAdapter = new ProgramAdapter(Collections.emptyList(), onClickListener);

        // Set program adapter as the adapter for RecyclerView.
        mRecycleView.setAdapter(mAdapter);

        getPrograms();
    }

    /**
     * Get next program list part and set it to adapter. Old data in adapter will be lost.
     */
    private void getPrograms() {
        mProgramPresenter.getPrograms(mNextPage, true)
                .compose(this.bindToLifecycle())
                .subscribe(programs -> {
                            mOverlayView.setVisibility(View.GONE);
                            mAdapter.setData(new ArrayList<>(Arrays.asList(programs)));
                        },
                        throwable -> {
                            Snackbar.make(mRecycleView, R.string.no_connection, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.no_connection_action, view -> {
                                        mOverlayView.setVisibility(View.VISIBLE);
                                        getPrograms();
                                    }).show();
                            Timber.e(throwable, "getPrograms(%d, %b) error", mNextPage, true);
                        });
        mNextPage++;
    }

    /**
     * Get next program list part and append it to adapter
     */
    private void addProgram() {
        mProgramPresenter.getPrograms(mNextPage, true)
                .compose(this.bindToLifecycle())
                .subscribe(programs -> {
                            mAdapter.addData(Arrays.asList(programs));
                            mRecycleView.addOnScrollListener(onScrollListener);
                        },
                        throwable -> {
                            Snackbar.make(mRecycleView, R.string.no_connection, Snackbar.LENGTH_LONG).show();
                            Timber.e(throwable, "getPrograms(%d, %b) error", mNextPage, true);
                        });
        mNextPage++;
    }
}
