package com.evgeny.echofm.ui.broadcast;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.presenter.FavouriteProgramPresenter;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.ui.program.ProgramArchiveFragment;
import com.evgeny.echofm.utils.DrawableUtils;
import com.evgeny.echofm.utils.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.evgeny.echofm.utils.TextUtils.getUrlFromImgTag;

public class BroadcastDetailActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout toolbarLayout;

    @BindView(R.id.image)
    ImageView imageView;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.tab_layout)
    TabLayout mTabLayout;

    @Inject
    BroadcastPresenter mBroadcastPresenter;

    @Inject
    FavouriteProgramPresenter mFavouriteProgramPresenter;

    @Inject
    SharedPreferences mSharedPreferences;

    CompositeDisposable disposables = new CompositeDisposable();

    /**
     * is this program are favourite
     */
    boolean isFavourite;

    /**
     * broadcast to show in this activity
     */
    private Broadcast mBroadcast;
    private Maybe<Broadcast> mBroadcastObservable;
    private ProgramArchiveFragment mArchiveFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_detail);

        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mArchiveFragment = ProgramArchiveFragment.newInstance(-1);

        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        // get and cache broadcast
        int id = intent.getIntExtra("id", -1);
        Timber.d("broadcast id = %d", id);
        showOverlay();

        mBroadcastObservable = mBroadcastPresenter.getBroadcast(id)
                .map(dbroadcast -> {
                    try {
                        return Broadcast.ADAPTER.decode(dbroadcast.broadcast());
                    } catch (IOException e) {
                        throw Exceptions.propagate(e);
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .cache();

        Disposable disposable = mBroadcastObservable.subscribe(broadcast -> {
            mBroadcast = broadcast;
            bindData();
            hideOverlay();
        }, throwable -> {
            showOverlay(R.string.overlay_message_error, R.drawable.ic_image_broken);
            Timber.e(throwable, "program fetch error id=%d", id);
        });
        disposables.add(disposable);

        disposable = mBroadcastObservable.map(broadcast -> broadcast.program)
                .flatMap(program -> mFavouriteProgramPresenter.isProgramFavourite(program.id))
                .compose(this.bindToLifecycle())
                .subscribe(rowNumb -> {
                    isFavourite = rowNumb > 0;
                    invalidateOptionsMenu();
                }, throwable -> {
                    showOverlay(R.string.overlay_message_error, R.drawable.ic_image_broken);
                    Timber.e(throwable, "program fetch error id=%d", id);
                });
        disposables.add(disposable);

        // init view pager
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new BroadcastDetailMainFragment(), getString(R.string.bcast_tab_about));
        adapter.addFragment(mArchiveFragment, getString(R.string.program_tab_archive));
        if (!mSharedPreferences.getBoolean(getString(R.string.show_bcast_comments_switch), false)) {
            adapter.addFragment(new BroadcastDetailCommentsFragment(), getString(R.string.bcast_tab_comments));
        }

        mViewPager.setAdapter(adapter);

        mTabLayout.setupWithViewPager(mViewPager);

    }

    public Maybe<Broadcast> getBroadcastObservable() {
        return mBroadcastObservable;
    }

    private void bindData() {
        String url = getUrlFromImgTag(mBroadcast.announce);

        if (url != null) {
            Glide.with(this)
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(imageView);
        }

        toolbarLayout.setTitle(mBroadcast.program.title);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        disposables.dispose();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_broadcast_detail, menu);

//        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_save_to_favourite);
        if (isFavourite) {
            menuItem.setIcon(R.drawable.ic_favorite);
        } else {
            menuItem.setIcon(R.drawable.ic_favorite_border);
        }

        // set tint according current theme(day or night)
        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.menu_share:
                TextUtils.shareText(this, mBroadcast.title + " " + mBroadcast.program.title
                );
                break;
            case R.id.action_save_to_favourite:
                if (mBroadcast.program != null) {
                    int snackbarMsgId;
                    if (isFavourite) {
                        snackbarMsgId = R.string.program_snackbar_favourite_removed;
                        mFavouriteProgramPresenter.removeProgram(mBroadcast.program.id);
                        isFavourite = false;
                    } else {
                        snackbarMsgId = R.string.program_snackbar_favourite_added;
                        mFavouriteProgramPresenter.insertProtoProgram(mBroadcast.program, mBroadcast.hosts);
                        isFavourite = true;
                    }

                    invalidateOptionsMenu();
                    Snackbar.make(mViewPager, snackbarMsgId, Snackbar.LENGTH_SHORT).show();
                }

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * View pager to present comments and info fragments
     */
    private static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
