package com.evgeny.echofm.ui.broadcast;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.ui.BaseActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BroadcastDownloadedActivity extends BaseActivity {

    public static final String TAG = "BcastDownloadedActivity";

    @Inject
    BroadcastPresenter mBroadcastPresenter;

    BroadcastAdapter mAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            if (mAdapter.getItemCount() == 0) showEmptyView();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_downloaded);

        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setHasFixedSize(true);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Set NewsAdapter as the adapter for RecyclerView.
        mAdapter = new BroadcastAdapter(this, mBroadcastPresenter);
        mRecyclerView.setAdapter(mAdapter);

        showEmptyView();

        // load broadcast list
        mBroadcastPresenter.getAllBroadcasts()
                .compose(this.bindToLifecycle())
                .subscribe(bcast -> {
                            if (bcast.size() > 0) hideOverlay();
                            mAdapter.setData(bcast);
                }, throwable -> Log.e(TAG, "load bcast list failed",throwable));

        mAdapter.registerAdapterDataObserver(adapterDataObserver);

    }

    @Override
    protected void onDestroy() {
        mAdapter.unregisterAdapterDataObserver(adapterDataObserver);

        super.onDestroy();
    }

    private void showEmptyView() {
        showOverlay(R.id.overlay_content, R.string.program_archive_empty_message, R.drawable.ic_file_download);
    }
}
