package com.evgeny.echofm.ui;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.evgeny.echofm.utils.FontStyle;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;

import timber.log.Timber;

public abstract class BaseActivity extends RxAppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String fontStyle = PreferenceManager.getDefaultSharedPreferences(this).getString("font_size",  FontStyle.Small.name());
        getTheme().applyStyle(FontStyle.valueOf(fontStyle).getResId(), true);
    }

    /**
     * show full screen overlay with the progress bar.
     */
    public void showOverlay() {
        displayOverlay(android.R.id.content, OverlayFragment.newInstance(-1, -1));
    }

    public void showOverlay(@StringRes int message, @DrawableRes int drawable) {
        displayOverlay(android.R.id.content, OverlayFragment.newInstance(message, drawable));
    }

    public void showOverlay(int contentId, @StringRes int message, @DrawableRes int drawable) {
        displayOverlay(contentId, OverlayFragment.newInstance(message, drawable));
    }

    private void displayOverlay(int containerId, OverlayFragment overlay) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        Fragment overlayFragment = getSupportFragmentManager().findFragmentByTag("overlay");
        if (overlayFragment != null) {
            Timber.d("showOverlay(): overlay already added");
            ft.remove(overlayFragment);
        }

        ft.add(containerId, overlay, "overlay").commitAllowingStateLoss();
    }


    /**
     * hide loading overlay
     */
    public void hideOverlay() {
        Fragment overlayFragment = getSupportFragmentManager().findFragmentByTag("overlay");
        if (overlayFragment != null)
            getSupportFragmentManager().beginTransaction()
                    .remove(overlayFragment).commitAllowingStateLoss();
    }
}
