package com.evgeny.echofm.ui.news;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.News;
import com.evgeny.echofm.presenter.NewsPresenter;
import com.evgeny.echofm.ui.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

/**
 * Show news list with refresh and loaad more news functionality.
 */
public class NewsListFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView mRecycleView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private NewsAdapter mAdapter;
    /**
     * last id in the list, used to load older news
     */
    private int lastNewsId = 0;

    @Inject
    NewsPresenter mNewsPresenter;
    private Unbinder unbinder;

    public NewsListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        mSwipeRefreshLayout.setOnRefreshListener(this::getNews);

        mAdapter = new NewsAdapter(new News[0]);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecycleView.setHasFixedSize(true);
        mRecycleView.setLayoutManager(layoutManager);
        mRecycleView.setAdapter(mAdapter);
        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mSwipeRefreshLayout.isRefreshing()) return;

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    getMoreNews();
                }
            }
        });

        // request news
        getNews();

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }


    /**
     * Request news started with latest one.
     */
    @SuppressLint("CheckResult")
    private void getNews() {
        mSwipeRefreshLayout.setRefreshing(true);
        mNewsPresenter.getLastNews()
                .compose(this.bindToLifecycle())
                .subscribe(news -> {
                    mAdapter.setData(news);
                    lastNewsId = news[news.length - 1].id;

                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> {
                    Timber.w(throwable, "getLastNews(): failed");
                    if (isVisible()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        Snackbar.make(getView(), R.string.no_connection, Snackbar.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * Request next news chunk
     */
    @SuppressLint("CheckResult")
    private void getMoreNews() {
        mNewsPresenter.getOlderNews(lastNewsId)
                .compose(this.bindToLifecycle())
                .subscribe(news -> {
                    mAdapter.appendData(news);
                    lastNewsId = news[news.length - 1].id;
                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Timber.e(throwable, "getOlderNews(%d)", lastNewsId);
                });
    }
}
