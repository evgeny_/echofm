package com.evgeny.echofm.ui.broadcast;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.BuildConfig;
import com.evgeny.echofm.R;
import com.evgeny.echofm.db.DownloadedBroadcast;
import com.evgeny.echofm.download.DownloadRequest;
import com.evgeny.echofm.download.Target;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.proto.Person;
import com.evgeny.echofm.ui.media.PlayerActivity;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.evgeny.echofm.utils.TextUtils.getUrlFromImgTag;
import static com.evgeny.echofm.utils.TextUtils.personList;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class BroadcastAdapter extends RecyclerView.Adapter<BroadcastAdapter.ViewHolder> {
    private static final String TAG = "BroadcastAdapter";

    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

    // Define listener member variable
    private OnItemClickListener listener;

    // Define the listener interface
    interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }

    private Activity hostActivity;
    private BroadcastPresenter broadcastPresenter;

    /**
     * map url to request and started request task
     */
    private Map<String, DownloadRequest> urlRequestMap = new HashMap<>();

    private List<DownloadedBroadcast> mDataSet;
    private final DownloadManager downloadManager;

    public BroadcastAdapter(Activity activity, BroadcastPresenter broadcastPresenter) {
        this.downloadManager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
        mDataSet = new ArrayList<>(0);
        hostActivity = activity;
        this.broadcastPresenter = broadcastPresenter;

        // define action buttons click listener
        this.listener = (itemView, position) -> {
            int itemId = itemView.getId();
            DownloadedBroadcast br = mDataSet.get(position);
            if (R.id.download == itemId) {
                handleDownloadClick(itemView, position);
            } else if (R.id.has_audio == itemId) {
                Log.d(TAG, "play button clicked at position: " + position);
                Intent activityIntent = new Intent(hostActivity, PlayerActivity.class);
                activityIntent.putExtra("broadcast_id", br.echoId());
                activityIntent.putExtra("is_downloaded", !TextUtils.isEmpty(br.filePath()));
                hostActivity.startActivity(activityIntent);
            }
        };
    }

    /**
     * Handle click on download/delete/cancel action-icon in the broadcast card
     * @param itemView the card view
     * @param position the position of the view in recycleview
     */
    private void handleDownloadClick(View itemView, int position) {
        Log.d(TAG, "download/remove/cancel button clicked at position: " + position);

        DownloadedBroadcast br = mDataSet.get(position);

        // delete record from db
        if (!TextUtils.isEmpty(br.filePath())) {
            Observable.defer(() -> Observable.just(broadcastPresenter.removeBroadcast(br)))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(bcast -> notifyItemChanged(position), throwable -> {
                        notifyItemChanged(position);
                        Timber.e(throwable, "error here");
                    });
        } else {
            try {
                final Broadcast b = Broadcast.ADAPTER.decode(br.broadcast());
                String url = b.sound.get(0).url;

                if (cancelDownloading(url)) {
                    ((Target)itemView).reset();
                } else {
                    // check permissions
                    int permissionCheck = ContextCompat.checkSelfPermission(hostActivity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (permissionCheck == android.content.pm.PackageManager.PERMISSION_DENIED) {
                        View contentView = hostActivity.findViewById(android.R.id.content);
                        Snackbar.make(contentView, R.string.perm_storage_explain, Snackbar.LENGTH_LONG).setAction(R.string.perm_storage_goto_settings,
                                view -> hostActivity.startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + BuildConfig.APPLICATION_ID)))).show();
                        return;
                    }

                    // start a new download
                    DownloadRequest downloadRequest = new DownloadRequest.Builder(downloadManager, Uri.parse(url))
                            .setNotificationTitle(hostActivity.getString(R.string.radio_name))
                            .setNotificationDescription(b.program.title)
                            .setTarget((Target) itemView)
                            .setOnFinishListener(downloadableResult -> {
                                DownloadedBroadcast bcast = mDataSet.get(position);
                                bcast.updateFilePath(downloadableResult.getLocalUri());

                                // persist downloaded bcast
                                broadcastPresenter.persistDownloadedBroadcast(bcast);

                                // update layout
                                itemView.post(() -> notifyItemChanged(position));
                            })
                            .execute();
                    itemView.setTag(url);
                    urlRequestMap.put(url, downloadRequest);
                }
            } catch (IOException e) {
                Timber.e(e, "failed to decode bast id = %d", br.echoId());
            }
        }
    }

    /**
     * Cancel downloading progress
     * @param url download url
     *
     * @return true if downloading was canceled, false if downloading process not exist
     */
    private boolean cancelDownloading(String url) {
        DownloadRequest request = urlRequestMap.get(url);

        if (request == null) return false;

        request.unbindTarget();
        request.cancelDownload();
        urlRequestMap.remove(url);
        return true;
    }

    void appendData(List<DownloadedBroadcast> data) {
        mDataSet.addAll(data);
        notifyDataSetChanged();
    }

    public void setData(List<DownloadedBroadcast> data) {
        mDataSet = data;
        notifyDataSetChanged();
    }

    private boolean isDownloadingRunning(String url) {
        DownloadRequest request = urlRequestMap.get(url);

        // download not started
        if (request == null) return false;

        int downloadStatus = request.getLastResult().getDownloadStatus();

        return downloadStatus == DownloadManager.STATUS_RUNNING;
    }

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.approved_at_date)
        TextView dateView;
        @BindView(R.id.approved_at_time)
        TextView timeView;
        @BindView(R.id.title)
        TextView titleView;
        @BindView(R.id.description)
        FrameLayout descriptionView;
        @BindView(R.id.announce)
        TextView announceView;
        @BindView(R.id.image)
        ImageView imageView;
        @BindView(R.id.program_title)
        TextView programTitleView;
        @BindView(R.id.guests_label)
        TextView guestsLabel;
        @BindView(R.id.guests)
        TextView guests;
        @BindView(R.id.hosts_label)
        TextView hostsLabel;
        @BindView(R.id.hosts)
        TextView hosts;
        @BindView(R.id.has_audio)
        ImageView hasAudio;
        @BindView(R.id.download)
        com.evgeny.echofm.ui.broadcast.PinProgressButton downloadButton;
        @BindView(R.id.divider)
        View buttonBarDivider;
        @BindView(R.id.space)
        View buttonBarSpace;

        public ViewHolder(View v, OnItemClickListener listener) {
            super(v);

            ButterKnife.bind(this, v);

            // open a detail broadcast activity on card click
            v.setOnClickListener(v1 -> {
                Timber.d("Element clicked.", getAdapterPosition());
                Intent intent = new Intent(v1.getContext(), BroadcastDetailActivity.class);
                int position = (Integer) v1.getTag();
                intent.putExtra("id", position);
                v1.getContext().startActivity(intent);
            });

            // add click event for download button
            downloadButton.setOnClickListener(button -> {
                int position = getAdapterPosition();
                listener.onItemClick(button, position);
            });

            // add click event for play button
            hasAudio.setOnClickListener(button -> {
                int position = getAdapterPosition();
                listener.onItemClick(button, position);
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.bcast_list_item, viewGroup, false);

        return new ViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        DownloadedBroadcast br = mDataSet.get(position);
        final Broadcast b;
        try {
            b = Broadcast.ADAPTER.decode(br.broadcast());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        // save position to get it from item on click event occur
        viewHolder.itemView.setTag(b.id);

        // date and time
        DateFormat approved = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
        try {
            Date d = approved.parse(b.approved_at);
            viewHolder.dateView.setText(dateFormatter.format(d));
            viewHolder.timeView.setText(timeFormatter.format(d));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // title program
        if (b.program != null) {
            viewHolder.programTitleView.setText(b.program.title);
        }

        // title broadcast
        String bcastTitle = Jsoup.parse(b.title).text();
        if (TextUtils.isEmpty(bcastTitle)) {
            viewHolder.titleView.setVisibility(View.GONE);
        } else {
            viewHolder.titleView.setVisibility(View.VISIBLE);
            viewHolder.titleView.setText(bcastTitle);
        }

        /* download broadcast button */
        boolean isSoundAvailable = b.has_audio && b.sound != null && b.sound.size() > 0;

        // set visibility of action buttons
        // both buttons are visibly only if a broadcast provide a sound record
        if (isSoundAvailable) {
            if (!TextUtils.isEmpty(br.filePath())) {
                viewHolder.downloadButton.setState(PinProgressButton.DrawableState.DELETE);
            }
        }

        setButtonBarVisibility(viewHolder, isSoundAvailable);

        // set state(delete or download) of the download button
        if (isSoundAvailable && TextUtils.isEmpty(br.filePath())) {
            String url = b.sound.get(0).url;
            DownloadRequest request = urlRequestMap.get(url);

            // there is a download task for this url
            // 1. if downloading this url is in progress, attach the button to download progress
            if (request != null) {
                if (isDownloadingRunning(url)) {
                    // resume
                    request.bindTarget(viewHolder.downloadButton);
                } else {
                    request.unbindTarget();
                    urlRequestMap.remove(url);
                    viewHolder.downloadButton.reset();
                }
                // if is there no download task for the url when
                // 1. if button is subscribed to some task, unsubscribe it
                // 2. reset button state eg set it to init state
            } else {
                // tag is the url of last subscription, if tag is null then request must be also null
                // this mean there is no previous subscription
                // otherwise get the task subscribed to and remove the subscription
                String urlTag = (String) viewHolder.downloadButton.getTag();
                DownloadRequest rq = urlRequestMap.get(urlTag);
                if (rq != null) {
                    rq.unbindTarget();
                }

                viewHolder.downloadButton.reset();
            }
        }

        // hosts
        List<Person> hosts = b.hosts;
        if (hosts.size() == 0) {
            viewHolder.hosts.setVisibility(View.GONE);
            viewHolder.hostsLabel.setVisibility(View.GONE);
        } else {
            viewHolder.hosts.setVisibility(View.VISIBLE);
            viewHolder.hostsLabel.setVisibility(View.VISIBLE);
            viewHolder.hosts.setText(personList(hosts));
        }

        //guests
        List<Person> guests = b.guests;
        if (guests.size() == 0) {
            viewHolder.guests.setVisibility(View.GONE);
            viewHolder.guestsLabel.setVisibility(View.GONE);
        } else {
            viewHolder.guests.setVisibility(View.VISIBLE);
            viewHolder.guestsLabel.setVisibility(View.VISIBLE);
            viewHolder.guests.setText(personList(guests));
        }

        /* description and image */
        if (TextUtils.isEmpty(b.announce)) {
            viewHolder.descriptionView.setVisibility(View.GONE);
        } else {
            viewHolder.descriptionView.setVisibility(View.VISIBLE);
        }

        String url = getUrlFromImgTag(b.announce);

        if (url == null) {
            viewHolder.announceView.setText(Html.fromHtml(b.announce));
            viewHolder.announceView.setVisibility(View.VISIBLE);
            viewHolder.imageView.setVisibility(View.GONE);
        } else {
            viewHolder.announceView.setVisibility(View.GONE);
            viewHolder.imageView.setVisibility(View.VISIBLE);
            Glide.with(viewHolder.imageView.getContext())
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(viewHolder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    /**
     * Set button bar view group visibility
     * @param viewHolder view holder
     * @param visible new state, then true is set to visible, to gone otherwise
     */
    private void setButtonBarVisibility(ViewHolder viewHolder, boolean visible) {
        if (visible) {
            viewHolder.hasAudio.setVisibility(View.VISIBLE);
            viewHolder.downloadButton.setVisibility(View.VISIBLE);
            viewHolder.buttonBarDivider.setVisibility(View.VISIBLE);
            viewHolder.buttonBarSpace.setVisibility(View.GONE);
        } else {
            viewHolder.hasAudio.setVisibility(View.GONE);
            viewHolder.downloadButton.setVisibility(View.GONE);
            viewHolder.buttonBarDivider.setVisibility(View.GONE);
            viewHolder.buttonBarSpace.setVisibility(View.VISIBLE);
        }
    }
}
