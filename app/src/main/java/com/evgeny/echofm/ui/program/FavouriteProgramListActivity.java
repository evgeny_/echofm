package com.evgeny.echofm.ui.program;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.presenter.FavouriteProgramPresenter;
import com.evgeny.echofm.ui.BaseActivity;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observables.ConnectableObservable;
import timber.log.Timber;

public class FavouriteProgramListActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecycleView;

    private ProgramAdapter mAdapter;

    private CompositeDisposable mSubscriptions;

    @Inject
    FavouriteProgramPresenter mFavouriteProgramPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_favourite_list);

        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        // set toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecycleView.setHasFixedSize(true);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));

        mAdapter = new ProgramAdapter(Collections.emptyList(), view -> {
            Program program = mAdapter.getItem((int) view.getTag());
            Intent i = new Intent(FavouriteProgramListActivity.this, ProgramDetailActivity.class);
            i.putExtra("id", program.id);
            startActivity(i);
        });

        // Set program adapter as the adapter for RecyclerView.
        mRecycleView.setAdapter(mAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showOverlay(R.id.overlay_content, R.string.program_archive_empty_message, R.drawable.ic_favorite_border);
        getPrograms();
    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (mSubscriptions != null) {
//            mSubscriptions.unsubscribe();
//        }
    }

    @OnClick(R.id.add_favourite)
    void onClickAddFavourite() {
        ProgramListActivity.startForSelect(this);
//        startActivity(new Intent(this, ProgramListActivity.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Get next program list part and set it to adapter. Old data in adapter will be lost.
     */
    private void getPrograms() {
        ConnectableObservable<List<Program>> observable =
                mFavouriteProgramPresenter.getAllPrograms()
                        .observeOn(AndroidSchedulers.mainThread())
                        .publish();


        mSubscriptions = new CompositeDisposable();

        mSubscriptions.add(observable.subscribe(mAdapter, throwable -> Timber.tag("FavouriteProgramListActivity").e("getPrograms() error: ", throwable)));

        mSubscriptions.add(observable.subscribe(programs -> {
            if (programs.size() > 0) hideOverlay();
        }, throwable -> Timber.tag("FavouriteProgramListActivity").e("getPrograms() error: ", throwable)));

        observable.connect();
    }
}
