package com.evgeny.echofm.ui.schedule;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.presenter.ISchedulerPresenter;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.ui.DividerItemDecoration;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class ScheduleActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    private SchedulerAdapter mAdapter;

    @Inject
    ISchedulerPresenter mSchedulerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider_horizontal)));

        mAdapter = new SchedulerAdapter(new Schedule[0]);

        // Set NewsAdapter as the adapter for RecyclerView.
        mRecyclerView.setAdapter(mAdapter);

        mSchedulerPresenter.getScheduleList()
                .compose(this.<Schedule[]>bindToLifecycle())
                .subscribe(mAdapter::setSchedule, throwable -> Timber.e(throwable, "Schedule can't be loaded"));
    }
}
