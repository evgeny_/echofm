/*
 * Copyright 2012 Roman Nurik
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.evgeny.echofm.ui.broadcast;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.CompoundButton;

import com.evgeny.echofm.R;
import com.evgeny.echofm.download.DownloadableResult;
import com.evgeny.echofm.download.Target;

import static com.evgeny.echofm.ui.broadcast.PinProgressButton.DrawableState.CANCEL;
import static com.evgeny.echofm.ui.broadcast.PinProgressButton.DrawableState.DELETE;
import static com.evgeny.echofm.ui.broadcast.PinProgressButton.DrawableState.DOWNLOAD;

/**
 * A two-state button that indicates whether some related content is pinned
 * (the checked state) or unpinned (the unchecked state), and the download
 * progress for this content.
 * <p>
 * See <a href="http://developer.android.com/design/building-blocks/progress.html#custom-indicators">Android
 * Design: Progress &amp; Activity</a> for more details on this custom
 * indicator.
 */
public class PinProgressButton extends CompoundButton implements Target {

    @IntDef({DOWNLOAD, CANCEL, DELETE})
    @interface DrawableState {
        int DOWNLOAD = 0;
        int CANCEL = 1;
        int DELETE = 2;
    }

    /**
     * current downloading progress
     */
    private int mProgress;

    //drawables
    //    private Drawable mShadowDrawable;
    private Drawable mDownloadDrawable;
    private Drawable mCancelDrawable;
    private Drawable mDeleteDrawable;

    private Paint mCirclePaint;
    private Paint mProgressPaint;
    private Rect mTempRect = new Rect();
    private RectF mTempRectF = new RectF();

    private int mDrawableSize;
    private int mInnerSize;

    @DrawableState
    private int mDrawableState;

    public PinProgressButton(Context context) {
        super(context);
        init(context, null, 0);
    }

    public PinProgressButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public PinProgressButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        mProgress = 0;

        final Resources res = getResources();
        int circleColor = ContextCompat.getColor(context, R.color.pin_progress_default_circle_color);
        int progressColor = ContextCompat.getColor(context, R.color.pin_progress_default_progress_color);

        if (attrs != null) {
            // Attribute initialization
            final TypedArray a = context
                    .obtainStyledAttributes(attrs, R.styleable.PinProgressButton,
                            defStyle, 0);

            mProgress = a.getInteger(R.styleable.PinProgressButton_progress, mProgress);

            circleColor = a.getColor(R.styleable.PinProgressButton_circleColor, circleColor);
            progressColor = a.getColor(R.styleable.PinProgressButton_progressColor, progressColor);

            a.recycle();
        }

        // Other initialization
        mCancelDrawable = ContextCompat.getDrawable(context, R.drawable.ic_clear_circular);
        mCancelDrawable.setCallback(this);
        mDownloadDrawable = ContextCompat.getDrawable(context, R.drawable.ic_file_download);
        mDownloadDrawable.setCallback(this);
        mDeleteDrawable = ContextCompat.getDrawable(context, R.drawable.ic_delete);
        mDeleteDrawable.setCallback(this);
//        mShadowDrawable = res.getDrawable(R.drawable.pin_progress_shadow);
//        mShadowDrawable.setCallback(this);

        mDrawableSize = mDownloadDrawable.getIntrinsicWidth();
        mInnerSize = res.getDimensionPixelSize(R.dimen.pin_progress_inner_size);

        mCirclePaint = new Paint();
        mCirclePaint.setColor(circleColor);
        mCirclePaint.setAntiAlias(true);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(progressColor);
        mProgressPaint.setAntiAlias(true);
    }

    /**
     * Returns the current download progress from 0 to max.
     */
    public int getProgress() {
        return mProgress;
    }

    /**
     * Sets the current download progress (between 0 and max).
     *
     */
    public void setProgress(int progress) {
        mProgress = progress;
        if (progress > 0 && progress < 100) setState(CANCEL);
        invalidate();
    }

    public void setState(@DrawableState int state) {
        if (state != mDrawableState) mDrawableState = state;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        // Try for a width based on our minimum
        int minw = getPaddingLeft() + getPaddingRight() + getSuggestedMinimumWidth();
        int w = resolveSizeAndState(minw, widthMeasureSpec, 1);

        int minh = getSuggestedMinimumHeight() + getPaddingBottom() + getPaddingTop();
        int h = resolveSizeAndState(minh, heightMeasureSpec, 1);

        setMeasuredDimension(w, h);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (mCancelDrawable.isStateful()) {
            mCancelDrawable.setState(getDrawableState());
        }
        if (mDownloadDrawable.isStateful()) {
            mDownloadDrawable.setState(getDrawableState());
        }

        if (mDeleteDrawable.isStateful()) mDeleteDrawable.setState(getDrawableState());
//        if (mShadowDrawable.isStateful()) {
//            mShadowDrawable.setState(getDrawableState());
//        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mTempRect.set(0, 0, mDrawableSize, mDrawableSize);
        mTempRect.offset((getWidth() - mDrawableSize) / 2, (getHeight() - mDrawableSize) / 2);

        mTempRectF.set(-0.5f, -0.5f, mInnerSize + 0.5f, mInnerSize + 0.5f);
        mTempRectF.offset((getWidth() - mInnerSize) / 2, (getHeight() - mInnerSize) / 2);

//        canvas.drawArc(mTempRectF, 0, 360, true, mCirclePaint);
        int angle = 360 * mProgress / 100;
        if (angle != 360) {
            canvas.drawArc(mTempRectF, -90, angle, true, mProgressPaint);
        }

        Drawable iconDrawable;
        switch (mDrawableState) {
            case DOWNLOAD:
                iconDrawable = mDownloadDrawable;
                break;
            case CANCEL:
                iconDrawable = mCancelDrawable;
                break;
            case DELETE:
                iconDrawable = mDeleteDrawable;
                break;
            default:
                Log.w("PinProgressButton", "onDraw() undefined drawable state:" + mDrawableState );
                iconDrawable = mDeleteDrawable;
        }
//        Log.d("TAG", "onDraw(): current progress=" + mProgress);
//        if (mProgress == 100) {
//            iconDrawable = mDeleteDrawable;
//        } else {
//            iconDrawable = isChecked() ? mCancelDrawable : mDownloadDrawable;
//        }

        iconDrawable.setBounds(mTempRect);
        iconDrawable.draw(canvas);

//        mShadowDrawable.setBounds(mTempRect);
//        mShadowDrawable.draw(canvas);
    }

    @Override
    public void updateProgress(DownloadableResult result) {
        setState(CANCEL);
        setProgress(result.getPercent());
    }

    @Override
    public void reset() {
        setState(DOWNLOAD);
        setProgress(0);
    }

    /**
     * A {@link Parcelable} representing the {@link PinProgressButton}'s state.
     */
    public static class SavedState extends BaseSavedState {
        private int mProgress;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            mProgress = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(mProgress);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        if (isSaveEnabled()) {
            SavedState ss = new SavedState(superState);
            ss.mProgress = mProgress;
            return ss;
        }
        return superState;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        mProgress = ss.mProgress;
    }
}
