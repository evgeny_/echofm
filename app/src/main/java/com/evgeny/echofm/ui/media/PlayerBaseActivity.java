package com.evgeny.echofm.ui.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;

import com.evgeny.echofm.service.PlayerService;
import com.evgeny.echofm.ui.BaseActivity;

public abstract class PlayerBaseActivity extends BaseActivity {

    private boolean mBound;
//    private PlayerService.PlayerServiceBinder playerServiceBinder;
    protected MediaControllerCompat mediaController;
//    protected boolean isConnected = false;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to BroadcastPlaybackService, cast the IBinder and get BroadcastPlaybackService instance
//            BroadcastPlaybackService.ServiceBinder binder = (BroadcastPlaybackService.ServiceBinder) service;
//            mService = binder.getService();
            mBound = true;

            PlayerService.PlayerServiceBinder playerServiceBinder = (PlayerService.PlayerServiceBinder) service;
            try {
                mediaController = new MediaControllerCompat(PlayerBaseActivity.this, playerServiceBinder.getMediaSessionToken());
                mediaController.registerCallback(
                        new MediaControllerCompat.Callback() {
                            @Override
                            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                                onPlaybackStateChange(state);
                            }
                        }
                );

//                isConnected = true;
                onConnected();
            }
            catch (RemoteException e) {
                mediaController = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
//            playerServiceBinder = null;
            mediaController = null;
            mBound = false;
        }
    };

    public abstract void onPlaybackStateChange(PlaybackStateCompat state);

    public abstract void onConnected();

    /**
     * Bind to player service
     */
    protected void bindService() {
        if (isFinishing()) return;
        
        // Bind to LocalService
        Intent intent = new Intent(this, PlayerService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }
}
