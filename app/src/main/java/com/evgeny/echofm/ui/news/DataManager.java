package com.evgeny.echofm.ui.news;

public interface DataManager<T> {
    void setData(T[] data);
    void appendData(T[] data);
}
