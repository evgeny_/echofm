package com.evgeny.echofm.ui.news;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evgeny.echofm.R;
import com.evgeny.echofm.model.News;
import com.evgeny.echofm.utils.ArrayUtils;

import java.text.SimpleDateFormat;
import java.util.Locale;

import timber.log.Timber;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements DataManager<News>{
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());

    private News[] mDataSet;

    @Override
    public void setData(News[] data) {
        mDataSet = data;
        notifyDataSetChanged();
    }

    @Override
    public void appendData(News[] data) {
        mDataSet = ArrayUtils.concat(mDataSet, data, News.class);
        notifyDataSetChanged();
    }

// BEGIN_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        private final TextView titleView;
        private final TextView descriptionView;
        private final TextView approvedAtTimeView;
        private final TextView approvedAtDateView;
        private final TextView commentsView;
        private final TextView viewsView;
        private final TextView likesView;

        public NewsViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(v1 -> {
                Timber.tag("NewsAdapter").d("Element %d clicked.", getAdapterPosition());
                Intent intent = new Intent(v1.getContext(), NewsDetailActivity.class);
                intent.putExtra("id", (Integer) v1.getTag());
                v1.getContext().startActivity(intent);
            });
            titleView = v.findViewById(R.id.title);
            descriptionView = v.findViewById(R.id.description);
            approvedAtTimeView = v.findViewById(R.id.approved_at_time);
            approvedAtDateView = v.findViewById(R.id.approved_at_date);
            commentsView = v.findViewById(R.id.comments);
            viewsView = v.findViewById(R.id.views);
            likesView = v.findViewById(R.id.likes);
        }

        TextView getTitleView() {
            return titleView;
        }

        TextView getDescriptionView() {
            return descriptionView;
        }

        TextView getApprovedAtDateView() {
            return approvedAtDateView;
        }

        TextView getApprovedAtTimeView() {
            return approvedAtTimeView;
        }

        TextView getCommentsView() {
            return commentsView;
        }

        TextView getViewsView() {
            return viewsView;
        }

        TextView getLikesView() {
            return likesView;
        }

        public void setPosition(int position) {
            itemView.setTag(position);
        }
    }

    /**
     * Initialize the data set of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    NewsAdapter(News[] dataSet) {
        mDataSet = dataSet;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.news_list_item, viewGroup, false);
        return new NewsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
            NewsViewHolder viewHolder = (NewsViewHolder) holder;
            viewHolder.setPosition(mDataSet[position].id);
            viewHolder.getTitleView().setText(mDataSet[position].title);
            viewHolder.getDescriptionView().setText(Html.fromHtml(mDataSet[position].announce));
            viewHolder.getApprovedAtTimeView().setText(timeFormatter.format(mDataSet[position].approved_at));
            viewHolder.getApprovedAtDateView().setText(dateFormatter.format(mDataSet[position].approved_at));
            viewHolder.getCommentsView().setText(String.valueOf(mDataSet[position].n_comments));
            viewHolder.getViewsView().setText(String.valueOf(mDataSet[position].n_views));
            viewHolder.getLikesView().setText(String.valueOf(mDataSet[position].n_likes));
    }

    @Override
    public int getItemCount() {
        return mDataSet.length;
    }
}
