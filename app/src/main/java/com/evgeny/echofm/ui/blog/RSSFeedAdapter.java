package com.evgeny.echofm.ui.blog;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.R;
import com.evgeny.echofm.network.FeedItem;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class RSSFeedAdapter extends RecyclerView.Adapter<RSSFeedAdapter.ViewHolder> {
//    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
//    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
//    private SimpleDateFormat dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
    //Thu, 31 Dec 2015 19:22:00 +0300

    private List<FeedItem> mDataSet;
//    private View view;

    public void setData(List<FeedItem> data) {
        mDataSet = data;
        notifyDataSetChanged();
    }

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView titleView;
        @BindView(R.id.description)
        TextView descriptionView;
        @BindView(R.id.avatar)
        ImageView avatarView;
        @BindView(R.id.author)
        TextView authorView;

        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);

            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Timber.d("Element %d clicked.", getAdapterPosition());

                    Uri uri = Uri.parse((String) v.getTag());

                    Intent i = new Intent(v.getContext(), BlogDetailActivity.class);
                    i.setData(uri);
                    v.getContext().startActivity(i);
//                    Intent intent = new Intent(v.getContext(), BlogDetailActivity.class);
//                    intent.putExtra("id", (Integer) v.getTag());
//                    v.getContext().startActivity(intent);
                }
            });
            descriptionView = (TextView) v.findViewById(R.id.description);
            avatarView = (ImageView) v.findViewById(R.id.avatar);
        }

        public TextView getTitleView() {
            return titleView;
        }

        public TextView getDescriptionView() {
            return descriptionView;
        }

        public ImageView getAvatarView() {
            return avatarView;
        }

        public void setLink(String link) {
            itemView.setTag(link);
        }

        public TextView getAuthorView() {
            return authorView;
        }
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public RSSFeedAdapter(List<FeedItem> dataSet) {
        mDataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.blog_list_item, viewGroup, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        FeedItem f = mDataSet.get(position);
//        viewHolder.setPosition(mDataSet[position].id);
        String[] firstLine = Html.fromHtml(f.getTitle()).toString().split(",");
        viewHolder.getAuthorView().setText(firstLine[0].trim());
        viewHolder.getTitleView().setText(firstLine[1].trim());
        Document html = Jsoup.parse(f.getDescription());

        viewHolder.getDescriptionView().setText(html.text());

        Glide.with(viewHolder.getAvatarView().getContext())
                .load(getImageSrc(html))
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(viewHolder.getAvatarView());
        viewHolder.setLink(f.getLink());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    private String getImageSrc(Document html) {
        Element image = html.select("img").first();

        return image == null ? null : image.absUrl("src");
    }
}
