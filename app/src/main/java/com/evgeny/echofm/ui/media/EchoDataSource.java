package com.evgeny.echofm.ui.media;

import android.net.Uri;

import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

import java.io.IOException;

import okhttp3.OkHttpClient;

public class EchoDataSource implements DataSource {

    private DataSource fileDataSource;
    private DataSource dataSource;

    @Override
    public long open(DataSpec dataSpec) throws IOException {
        Assertions.checkState(dataSource == null);
        // Choose the correct source for the scheme.
        if (Util.isLocalFileUri(dataSpec.uri)) {
            dataSource = getFileDataSource();
        } else {
            dataSource = getHttpDataSource();
        }
        // Open the source and return.
        return dataSource.open(dataSpec);
    }

    @Override
    public int read(byte[] buffer, int offset, int readLength) throws IOException {
        return dataSource.read(buffer, offset, readLength);
    }

    @Override
    public Uri getUri() {
        return dataSource == null ? null : dataSource.getUri();
    }

    @Override
    public void close() throws IOException {
        if (dataSource != null) {
            try {
                dataSource.close();
            } finally {
                dataSource = null;
            }
        }
    }

    private DataSource getFileDataSource() {
        if (fileDataSource == null) {
            fileDataSource = new FileDataSource(null);
        }
        return fileDataSource;
    }

    private DataSource getHttpDataSource() {
        return new OkHttpDataSource(new OkHttpClient(), "echofm/android", null, null, null,
                new HttpDataSource.RequestProperties());
    }
}
