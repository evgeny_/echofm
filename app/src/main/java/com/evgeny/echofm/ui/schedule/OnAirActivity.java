package com.evgeny.echofm.ui.schedule;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.live.EchoLiveStreamRepository;
import com.evgeny.echofm.live.LiveStreamManager;
import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.presenter.ISchedulerPresenter;
import com.evgeny.echofm.service.PlayerService;
import com.evgeny.echofm.ui.BaseActivity;
import com.evgeny.echofm.ui.DividerItemDecoration;
import com.evgeny.echofm.utils.DrawableUtils;
import com.evgeny.echofm.utils.TimeUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class OnAirActivity extends BaseActivity {

    @Inject
    ISchedulerPresenter mSchedulerPresenter;

    @Inject
    SharedPreferences mSharedPreference;

    @BindView(R.id.title)
    TextView mTitleView;

    @BindView(R.id.program_title)
    TextView mProgramTitleView;

    @BindView(R.id.hosts)
    TextView mHostsView;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.time_elapsed)
    TextView mElapsedTime;

    @BindView(R.id.time_remain)
    TextView mRemainedTime;

    @BindView(R.id.seekbar)
    SeekBar mSeekBar;

    private SchedulerAdapter mAdapter;
    private long startTime;
    private long endTime;
    private CountDownTimer seekBarTimer;
    private int isPlaying;

    private boolean mBound;

//    private View mPlayActionView;
    @Inject
    SharedPreferences mSharedPreferences;

    /**
     * parametrized string to format elapsed and past time
     */
    private String mDurationFormat;
    private LiveStreamManager liveStreamManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_air);

        ButterKnife.bind(this);

        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider_horizontal)));

        mAdapter = new SchedulerAdapter(new Schedule[0]);

        mRecyclerView.setAdapter(mAdapter);

        refreshSchedule(true);

        liveStreamManager = new LiveStreamManager(new EchoLiveStreamRepository(this, mSharedPreferences));
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Bind to LocalService
        Intent intent = new Intent(this, PlayerService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Unbind from the service
        if (mBound) {
            unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_on_air, menu);
        DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(this, R.color.colorPrimaryIcon));
        menu.findItem(R.id.action_play).getActionView().setOnClickListener(v -> {
            if (!mBound) return; // ignore click if service not bounded

            if (isPlaying == 0) animatePlayButton(v);

            // send action to playback service
            if (isPlaying == 1) {
                mediaController.getTransportControls().stop();
            } else {
                playLiveStream(Uri.parse(liveStreamManager.getSelectedStream().getUrl()));
            }
        });
        return true;
    }

    private void animatePlayButton(View v) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(v, "alpha", 0f, 1f);
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setDuration(1000);
        animator.start();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_play);
        ImageView actionView = (ImageView) menuItem.getActionView();
        if (isPlaying == 1) {
            actionView.setImageResource(R.drawable.ic_pause);
        } else {
            actionView.setImageResource(R.drawable.ic_play);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_select_stream:
                createStreamSelectDialog().show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void refreshSchedule(boolean showOverlay) {
        if (showOverlay) showOverlay();
        mSchedulerPresenter.getProgramsAfterNow()
                .compose(this.bindToLifecycle())
                .subscribe(this::bindSchedule, throwable -> {
                    hideOverlay();
                    Timber.e(throwable, "get schedules error");
                });
    }

    /**
     * Bind fetch schedule data to UI
     *
     * @param schedules current schedule
     */
    private void bindSchedule(ArrayList<Schedule> schedules) {
        hideOverlay();
        if (schedules == null || schedules.isEmpty()) {
            Timber.i("schedule is empty");
            return;
        }

        // set list
        mAdapter.setSchedule(schedules.toArray(new Schedule[schedules.size()]));

        // set current program info
        Schedule currentSchedule = schedules.get(0);
        if (!TextUtils.isEmpty(currentSchedule.title)) {
            mTitleView.setText(Html.fromHtml(currentSchedule.title));
            mTitleView.setVisibility(View.VISIBLE);
        } else {
            mTitleView.setVisibility(View.GONE);
        }

        if (currentSchedule.program != null) {
            mProgramTitleView.setText(currentSchedule.program.title);
            mProgramTitleView.setVisibility(View.VISIBLE);
        } else {
            mProgramTitleView.setVisibility(View.GONE);
        }

        if (currentSchedule.person != null) {
            mHostsView.setText(getString(R.string.program_hosts, currentSchedule.person.toString()));
            mHostsView.setVisibility(View.VISIBLE);
        } else {
            mHostsView.setVisibility(View.GONE);
        }

        // set seekbar
        if (schedules.size() >= 2) {
            startTime = currentSchedule.approved_at.getTime();
            endTime = schedules.get(1).approved_at.getTime();

            int duration = (int) ((endTime - startTime) / 1000);
            mSeekBar.setMax(duration);

            // get duration format depends on duration(if duration is more then a hour)
            mDurationFormat = getResources().getString(
                    duration < 3600 ? R.string.durationformatshort : R.string.durationformatlong);

            if (seekBarTimer != null) seekBarTimer.cancel();
            seekBarTimer = new CountDownTimer(endTime - System.currentTimeMillis(), 1000) {

                @Override
                public void onTick(long millisUntilFinished) {
                    updateSeekbar();
                }

                @Override
                public void onFinish() {
                    // get new program list and refresh layout data binding
                    refreshSchedule(false);
                }
            }.start();
        }
    }

    @OnClick(R.id.on_air_call)
    void onClickCall() {
        Intent intent = new Intent(Intent.ACTION_DIAL,
                Uri.fromParts("tel", getString(R.string.on_air_phone_number), null));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.on_air_call_not_available, Snackbar.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.on_air_sms)
    void onClickSMS() {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.fromParts("sms", getString(R.string.on_air_sms_number), null));

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Snackbar.make(findViewById(android.R.id.content), R.string.on_air_sms_not_available, Snackbar.LENGTH_SHORT).show();
        }
    }

    /**
     * Set seekbar position. Set remain and elapsed time.
     */
    private void updateSeekbar() {
        long cm = System.currentTimeMillis();

        long pp1 = (cm - startTime) / 1000;
        long pp2 = (endTime - cm) / 1000;

        mElapsedTime.setText(TimeUtils.makeShortTimeString(mDurationFormat, pp1));
        mRemainedTime.setText(TimeUtils.makeShortTimeString(mDurationFormat, pp2));
        mSeekBar.setProgress((int) pp1);
    }

    private PlayerService.PlayerServiceBinder playerServiceBinder;
    private MediaControllerCompat mediaController;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mBound = true;

            playerServiceBinder = (PlayerService.PlayerServiceBinder) service;
            try {
                mediaController = new MediaControllerCompat(
                        OnAirActivity.this, playerServiceBinder.getMediaSessionToken());
                mediaController.registerCallback(
                        new MediaControllerCompat.Callback() {
                            @Override
                            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                                if (state == null) return;
                                isPlaying = state.getState() == PlaybackStateCompat.STATE_PLAYING ? 1 : 0;
                                invalidateOptionsMenu();
//                                playButton.setEnabled(!playing);
//                                pauseButton.setEnabled(playing);
//                                stopButton.setEnabled(playing);
                            }
                        }
                );
            }
            catch (RemoteException e) {
                mediaController = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            playerServiceBinder = null;
            mediaController = null;
            mBound = false;
        }
    };

    /**
     * Create choose stream dialog.
     *
     * @return dialog
     */
    private AlertDialog createStreamSelectDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Set the dialog title
        builder.setTitle(R.string.stream_dialog_title)
                .setSingleChoiceItems(liveStreamManager.getNameArray(), liveStreamManager.getSelectedStream().getId(), (dialog, which) -> {
                    liveStreamManager.memorizeStream(liveStreamManager.getStreams().get(which));
                    if (mBound && isPlaying == 1) {
                        playLiveStream(Uri.parse(liveStreamManager.getSelectedStream().getUrl()));
                        //mService.stop(false);
                        //mService.playLive();
                    }

                    dialog.dismiss();
                })
                .setNegativeButton(R.string.stream_dialog_button, (dialog, which) -> dialog.dismiss());

        return builder.create();
    }

    private void playLiveStream(Uri uri) {
        Bundle extras = new Bundle();
        extras.putString("stream_mode", "live");
        mediaController.getTransportControls().playFromUri(uri, extras);
    }
}
