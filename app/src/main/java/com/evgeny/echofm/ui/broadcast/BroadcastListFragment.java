package com.evgeny.echofm.ui.broadcast;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.ui.BaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class BroadcastListFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView mRecycleView;

    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout mSwipeRefreshLayout;

    BroadcastAdapter mAdapter;

    @Inject
    BroadcastPresenter mBroadcastPresenter;

    /**
     * last id in the list, used to load older broadcasts
     */
    private int lastBcastId = 0;
    private Unbinder unbinder;


    public BroadcastListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bcast_list, container, false);
        unbinder = ButterKnife.bind(this, view);

        mRecycleView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecycleView.setLayoutManager(layoutManager);

        // Set NewsAdapter as the adapter for RecyclerView.
        mAdapter = new BroadcastAdapter(getActivity(), mBroadcastPresenter);
        mRecycleView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this::getBroadcasts);
        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (mSwipeRefreshLayout.isRefreshing()) return;

                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    mSwipeRefreshLayout.setRefreshing(true);
                    getMoreBroadcasts();
                }
            }
        });

        /* request broadcasts */
        getBroadcasts();
        return view;
    }


    /**
     * Fetch a broadcast list and append it to a list adapter
     */
    private void getBroadcasts() {
        mBroadcastPresenter.getBroadcasts()
                .compose(this.bindToLifecycle())
                .subscribe(broadcasts -> {
                    mAdapter.setData(broadcasts);
                    lastBcastId = (int) broadcasts.get(broadcasts.size() - 1).echoId();
                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> {
                    Timber.w(throwable, "getFirstPage(): failed");
                    if (isVisible()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
    }

    /**
     * Fetch next broadcast chunk.
     */
    private void getMoreBroadcasts() {
        mBroadcastPresenter.getOlderBroadcasts(lastBcastId)
                .compose(this.bindToLifecycle())
                .subscribe(broadcasts -> {
                    mAdapter.appendData(broadcasts);
                    lastBcastId = (int) broadcasts.get(broadcasts.size() -1 ).echoId();
                    mSwipeRefreshLayout.setRefreshing(false);
                }, throwable -> {
                    mSwipeRefreshLayout.setRefreshing(false);
                    Timber.e(throwable, "getMoreBroadcast()");
                    Snackbar.make(getView(), R.string.no_connection, Snackbar.LENGTH_LONG)
                            .setAction(R.string.no_connection_action, view -> {
                                getBroadcasts();
                            }).show();
                });
    }

    /**
     * Show snackbar with action button if write_to_external_storage permission missed.
     *
     * @param view ancestor view
     */
    private void showNoPermissionSnackbar(View view) {
        Snackbar.make(view, R.string.no_write_external_storage_permission_msg, Snackbar.LENGTH_LONG)
                .setAction(R.string.no_write_external_storage_permission_btn, view1 -> {
                    Context context = getActivity();
                    final Intent i = new Intent();
                    i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    i.addCategory(Intent.CATEGORY_DEFAULT);
                    i.setData(Uri.parse("package:" + context.getPackageName()));
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    context.startActivity(i);
                })
                .show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }
}
