package com.evgeny.echofm.ui;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.evgeny.echofm.R;
import com.evgeny.echofm.utils.DrawableUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.evgeny.echofm.R.color.colorPrimaryIcon;

/**
 * Fragment shown during long running process
 */
public class OverlayFragment extends Fragment {
    @BindView(R.id.overlay_image)
    ImageView mStateIcon;

    @BindView(R.id.overlay_message)
    TextView mMessageView;

    @BindView(R.id.overlay_progress_bar)
    ProgressBar mProgressView;


    public OverlayFragment() {
        // Required empty public constructor
    }

    public static OverlayFragment newInstance(@StringRes int message, @DrawableRes int drawable) {
        OverlayFragment overlayFragment = new OverlayFragment();
        Bundle args = new Bundle(2);
        args.putInt("message", message);
        args.putInt("icon", drawable);

        overlayFragment.setArguments(args);
        return overlayFragment;
    }

    public static OverlayFragment showProgress(@StringRes int message) {
        OverlayFragment overlayFragment = new OverlayFragment();
        Bundle args = new Bundle(1);
        args.putInt("message", message);

        overlayFragment.setArguments(args);
        return overlayFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overlay, container, false);
        ButterKnife.bind(this, view);

        Bundle args = getArguments();
        if (args != null) {
            int messageRes = args.getInt("message", -1);
            if (messageRes != -1) mMessageView.setText(messageRes);

            int iconRes = args.getInt("icon", -1);
            if (iconRes != -1) {
                mProgressView.setVisibility(View.GONE);
                Drawable drawable = ContextCompat.getDrawable(getActivity(), iconRes);
                mStateIcon.setImageDrawable(DrawableUtils.tintDrawable(drawable, ContextCompat.getColor(getActivity(), colorPrimaryIcon)));
            }
        }

        return view;
    }

}
