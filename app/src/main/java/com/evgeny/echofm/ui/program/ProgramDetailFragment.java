package com.evgeny.echofm.ui.program;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Person;
import com.evgeny.echofm.model.Program;
import com.evgeny.echofm.ui.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProgramDetailFragment extends BaseFragment {


    @BindView(R.id.hosts)
    LinearLayout mHostsLayout;

    @BindView(R.id.guests)
    LinearLayout mGuestsLayout;

    @BindView(R.id.program_title)
    TextView mProgramTitle;

    @BindView(R.id.program_schedule)
    TextView mProgramSchedule;

    public ProgramDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_program_detail, container, false);
        ButterKnife.bind(this, view);

        // get program
        if (getActivity() instanceof ProgramDetailActivity) {
            ((ProgramDetailActivity) getActivity()).mProgramObservable
                    .compose(this.bindToLifecycle())
                    .subscribe(this::bindData, throwable -> {
                        Timber.w(throwable, "broadcast request error");
                    });
        }

        return view;
    }

    /**
     * @param program program to display
     */
    private void bindData(Program program) {
        Timber.d("program name to display=%s", program.title);

        mProgramSchedule.setText(program.schedule);
        mProgramTitle.setText(program.title);
        if (program.getHosts().length > 0) {
            for (Person person : program.getHosts()) {
                addPerson(person, true, mHostsLayout);
            }
        }

        if (program.getGuests().length > 0) {
            for (Person person : program.getGuests()) {
                addPerson(person, false, mGuestsLayout);
            }
        }
    }

    private void addPerson(Person person, boolean isHost, ViewGroup parent) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.layout_person, null);
        ((TextView) ll.findViewById(R.id.name)).setText(getString(R.string.full_name, person.i, person.f));
        ((TextView) ll.findViewById(R.id.role)).setText(isHost ? getString(R.string.host) : getString(R.string.guest));
        ((TextView) ll.findViewById(R.id.position)).setText(person.position);

        if (person.avatar != null) {
            Glide.with(this).load(person.avatar.url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into((ImageView) ll.findViewById(R.id.avatar));
        }
        parent.addView(ll);
    }

}
