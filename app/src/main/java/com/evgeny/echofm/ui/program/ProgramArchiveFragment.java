package com.evgeny.echofm.ui.program;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.ui.BaseFragment;
import com.evgeny.echofm.ui.broadcast.BroadcastAdapter;
import com.evgeny.echofm.ui.broadcast.BroadcastDetailActivity;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Maybe;
import timber.log.Timber;

public class ProgramArchiveFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView mArchiveList;

    @BindView(R.id.overlay)
    View overlay;

    @Inject
    BroadcastPresenter mBroadcastPresenter;

    BroadcastAdapter mAdapter;

    private int mId;

    private Unbinder mUnbinder;

    public ProgramArchiveFragment() {
        // Required empty public constructor
    }

    public static ProgramArchiveFragment newInstance(int programId) {
        ProgramArchiveFragment fragment = new ProgramArchiveFragment();
        Bundle args = new Bundle(1);
        args.putInt("id", programId);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mId = getArguments().getInt("id");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_program_archive, container, false);
        mUnbinder = ButterKnife.bind(this, view);

        ((EchoApplication) getActivity().getApplication()).getNetComponent().inject(this);

        // init program archive list view
        mArchiveList.setHasFixedSize(true);
        mArchiveList.setLayoutManager(new LinearLayoutManager(getActivity()));

        // Set NewsAdapter as the adapter for RecyclerView.
        mAdapter = new BroadcastAdapter(getActivity(), mBroadcastPresenter);
        mArchiveList.setAdapter(mAdapter);

        // get archive
        if (getActivity() instanceof ProgramDetailActivity) {
            mBroadcastPresenter.getBroadcastArchive(String.valueOf(mId))
                    .compose(this.bindToLifecycle())
                    .subscribe(broadcasts -> {
                        mAdapter.setData(broadcasts);
                        overlay.setVisibility(View.GONE);
                    }, throwable -> Timber.e(throwable, "getProgramArchive(%d) error", mId));
        } else if (getActivity() instanceof BroadcastDetailActivity) {
            Maybe<Broadcast> observable = ((BroadcastDetailActivity) getActivity()).getBroadcastObservable();
            observable.flatMap(broadcast -> mBroadcastPresenter.getBroadcastArchive(String.valueOf(broadcast.program.id)))
                    .compose(this.bindToLifecycle())
                    .subscribe(broadcasts -> {
                        mAdapter.setData(broadcasts);
                        overlay.setVisibility(View.GONE);
                    }, throwable -> Timber.e(throwable, "getProgramArchive() error"));
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mUnbinder.unbind();
    }
}
