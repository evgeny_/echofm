package com.evgeny.echofm.ui.media;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.EchoApplication;
import com.evgeny.echofm.R;
import com.evgeny.echofm.db.DownloadedBroadcast;
import com.evgeny.echofm.presenter.BroadcastPresenter;
import com.evgeny.echofm.proto.Broadcast;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.Maybe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

import static com.evgeny.echofm.utils.TextUtils.getUrlFromImgTag;

//import com.evgeny.echofm.eventbus.MediaEventMessage;

public class PlayerActivity extends PlayerBaseActivity {

    @BindView(R.id.album_art)
    ImageView mProgramArt;

    @BindView(R.id.song_title)
    TextView mSongTitleView;

    @BindView(R.id.song_artist)
    TextView mSongArtistView;

    @BindView(R.id.song_progress)
    SeekBar mSongProgressBar;

    @BindView(R.id.song_elapsed_time)
    TextView mSongElapsedTimeView;

    @BindView(R.id.song_duration)
    TextView mSongDurationView;

    @BindView(R.id.previous)
    ImageButton mFastBackwardButton;

    @BindView(R.id.next)
    ImageButton mFastForwardButton;

    @BindView(R.id.playpausefloating)
    FloatingActionButton mPlayPauseButton;

    private PlayPauseDrawable playPauseDrawable = new PlayPauseDrawable();

    @Inject
    BroadcastPresenter mBroadcastPresenter;
    private Broadcast mBroadcast;

//    private Pattern imageSrcPattern = Pattern.compile("src=\"[^\"]*\"", Pattern.CASE_INSENSITIVE);
    private SimpleDateFormat mTimeFormatHours = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    private SimpleDateFormat mTimeFormatMinutes = new SimpleDateFormat("mm:ss", Locale.getDefault());
    private Date mProgressDateTime;
    private long mBroadcastId;
    private boolean isDownloaded;
    private String broadcastPath;
    private PlaybackStateCompat state;
    private long lastPosition;

    private Flowable<Long> mPlaybackProgressObservable = Flowable.interval(1, TimeUnit.SECONDS)
            .onBackpressureLatest();
    private Disposable mPositionObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        ButterKnife.bind(this);
        ((EchoApplication) getApplication()).getNetComponent().inject(this);

        // set action bar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        showOverlay();

        mBroadcastId = getIntent().getLongExtra("broadcast_id", -1);
        isDownloaded = getIntent().getBooleanExtra("is_downloaded", false);
        Timber.d("broadcast id = %d", mBroadcastId);
        if (mBroadcastId == -1) {
            // TODO show error here
            return;
        }

        // init elapsed date
        mProgressDateTime = new Date(0L);

        // get broadcast details
        Maybe<DownloadedBroadcast> obs;
        if (isDownloaded) {
            obs = mBroadcastPresenter.getDownloadedBroadcast(mBroadcastId);
        } else {
            obs = mBroadcastPresenter.getBroadcast(mBroadcastId);
        }

        obs.subscribeOn(Schedulers.io())
                .compose(this.bindToLifecycle())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(broadcast -> {
                    try {
                        mBroadcast = Broadcast.ADAPTER.decode(broadcast.broadcast());
                    } catch (IOException e) {
                        mBroadcast = null;
                    }
                    broadcastPath = broadcast.filePath();
                    lastPosition = broadcast.timePlayed();
                    bindData();

                    bindService();
                }, throwable -> Timber.w(throwable, "broadcast request error, id=%d", mBroadcastId));
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Bind fetched broadcast to UI
     */
    private void bindData() {
        // set play button state
        mPlayPauseButton.setImageDrawable(playPauseDrawable);

        String url = getUrlFromImgTag(mBroadcast.announce);

        if (url != null) {
            Glide.with(this)
                    .load(url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into(mProgramArt);
        }

        if (mBroadcast.program != null && !TextUtils.isEmpty(mBroadcast.program.title)) {
            mSongTitleView.setText(Html.fromHtml(mBroadcast.program.title));
        } else {
            mSongTitleView.setText(R.string.app_name);
        }

        mSongArtistView.setText(Html.fromHtml(mBroadcast.title));

        LocalTime time = new LocalTime(0, 0); // midnight
        time = time.plusSeconds(mBroadcast.sound.get(0).duration);
        String formattedDuration;
        if (time.getHourOfDay() > 0) {
            formattedDuration = DateTimeFormat.forPattern("HH:mm:ss").print(time);
        } else {
            formattedDuration = DateTimeFormat.forPattern("mm:ss").print(time);
        }
        mSongDurationView.setText(formattedDuration);
        setSeekBar();

        hideOverlay();
    }

    private void setSeekBar() {
        int maxProgress = mBroadcast.sound.get(0).duration;
        mSongProgressBar.setMax(maxProgress);
        mSongProgressBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaController.getTransportControls().seekTo(seekBar.getProgress() * 1000);
            }
        });
    }

    @Override
    public void onPlaybackStateChange(PlaybackStateCompat state) {
        this.state = state;

        if (state.getState() == PlaybackStateCompat.STATE_PLAYING) {
            playPauseDrawable.transformToPause(true);
            startObserveProgress();
        } else {
            playPauseDrawable.transformToPlay(true);
        }

        if (state.getState() == PlaybackStateCompat.STATE_STOPPED) {
            stopObserverProgress();
        }
    }

    @Override
    public void onConnected() {
        // start playing
        Bundle extras = new Bundle();
        extras.putParcelable("media_metadata", createMetadata(mBroadcast));
        extras.putString("stream_mode", "broadcast");
        extras.putLong("broadcast_id", mBroadcastId);
        extras.putBoolean("is_downloaded", isDownloaded);

        mediaController.getTransportControls().seekTo(lastPosition);
        Uri uri = isDownloaded ? Uri.parse(broadcastPath) : Uri.parse(mBroadcast.sound.get(0).url);
        mediaController.getTransportControls().playFromUri(uri, extras);
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopObserverProgress();
    }

    @OnClick(R.id.playpausefloating)
    public void onClickPlayButton(View button) {
        if (state.getState() == PlaybackStateCompat.STATE_PLAYING) {
            mediaController.getTransportControls().pause();
        } else {
            mediaController.getTransportControls().play();
        }
    }

    @OnClick(R.id.next)
    public void onClickNextButton(View button) {
        mediaController.getTransportControls().seekTo(mediaController.getPlaybackState().getPosition() + 5000);
    }

    @OnClick(R.id.previous)
    public void onClickPreviousButton(View button) {
        mediaController.getTransportControls().seekTo(mediaController.getPlaybackState().getPosition() - 5000);
    }

    private void startObserveProgress() {
        // more then one hour
        if (mPositionObserver == null) {
            mPositionObserver = mPlaybackProgressObservable
                    .filter(interval -> mediaController != null)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(next -> {
                        int position = (int) (mediaController.getPlaybackState().getPosition());
                        setStreamProgress(position);
                    }, throwable -> Timber.e(throwable, "playback progress observable error"));
        }
    }

    private void stopObserverProgress() {
        if (mPositionObserver != null) {
            mPositionObserver.dispose();
            mPositionObserver = null;
        }
    }

    /**
     * Set progress bar position and elapsed time depend on position in playing stream.
     *
     * @param position position in stream
     */
    private synchronized void setStreamProgress(int position) {
        mSongProgressBar.setProgress(position / 1000);
        mProgressDateTime.setTime(position);
        if (position >= 3600000) { // more then one hour
            mSongElapsedTimeView.setText(mTimeFormatHours.format(mProgressDateTime));
        } else {
            mSongElapsedTimeView.setText(mTimeFormatMinutes.format(mProgressDateTime));
        }
    }

    private MediaMetadataCompat createMetadata(Broadcast broadcast) {
        MediaMetadataCompat.Builder metadataBuilder = new MediaMetadataCompat.Builder();

        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, String.valueOf(broadcast.id));
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_TITLE, broadcast.program == null ? broadcast.title : broadcast.program.title);
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, broadcast.program == null || broadcast.program.picture == null ? "" : broadcast.program.picture.url);

        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ALBUM, getString(R.string.radio_name));
        metadataBuilder.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "");
        metadataBuilder.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, -1);

        return metadataBuilder.build();
    }
}
