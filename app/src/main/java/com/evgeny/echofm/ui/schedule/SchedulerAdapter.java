package com.evgeny.echofm.ui.schedule;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.evgeny.echofm.R;
import com.evgeny.echofm.model.Person;
import com.evgeny.echofm.model.Schedule;
import com.evgeny.echofm.ui.program.ProgramDetailActivity;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.evgeny.echofm.utils.TextUtils.personList;

public class SchedulerAdapter extends RecyclerView.Adapter<SchedulerAdapter.ViewHolder> {

    private Schedule[] mSchedule;
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());

    public SchedulerAdapter(Schedule[] schedules) {
        mSchedule = schedules;
    }

    public void setSchedule(Schedule[] schedule) {
        mSchedule = schedule;
        notifyDataSetChanged();
    }

//    public int getLiveSchedPosition() {
//        for (int i = 0; i < mSchedule.length; i++) {
//            DateTime dateTime = new DateTime(mSchedule[i].approved_at);
//            if (i > 0 && dateTime.isAfterNow()) {
//                Log.d(TAG, "current schedules=" + mSchedule[i - 1].title);
//                return (i - 1);
//            }
//        }
//
//        return 0; // fallback
//    }

    @Override
    public SchedulerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sched_simple_list_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return mSchedule[position].bcast == null ? R.id.recyclerView : R.id.action_bcast;
    }

    @Override
    public void onBindViewHolder(SchedulerAdapter.ViewHolder viewHolder, int position) {
        Schedule s = mSchedule[position];

        if (s.program == null) {
            viewHolder.setId(-1);
        } else {
            viewHolder.setId(s.program.id);
        }

        if (TextUtils.isEmpty(s.title)) {
            viewHolder.title.setVisibility(View.GONE);
        } else {
            viewHolder.title.setVisibility(View.VISIBLE);
            viewHolder.title.setText(Html.fromHtml(s.title));
        }

        /** program fields*/
        if (s.program != null) {
            if (TextUtils.isEmpty(s.program.title)) {
                viewHolder.programTitle.setVisibility(View.GONE);
            } else {
                viewHolder.programTitle.setVisibility(View.VISIBLE);
                viewHolder.programTitle.setText(Html.fromHtml(s.program.title));
            }

            if (TextUtils.isEmpty(s.program.announce)) {
                viewHolder.programAnnounce.setVisibility(View.GONE);
            } else {
                viewHolder.programAnnounce.setText(Html.fromHtml(s.program.announce));
                viewHolder.programAnnounce.setVisibility(View.VISIBLE);
            }
        }

        /** broadcast fields */
        if (s.bcast != null) {
            /** hosts */
            Person[] hosts = s.bcast.getHosts();
            if (hosts.length == 0) {
                ((View) viewHolder.hosts.getParent()).setVisibility(View.GONE);
            } else {
                ((View) viewHolder.hosts.getParent()).setVisibility(View.VISIBLE);
                viewHolder.hosts.setText(personList(hosts));
            }

            /** guests */
            Person[] guests = s.bcast.getGuests();
            if (guests.length == 0) {
                ((View) viewHolder.guests.getParent()).setVisibility(View.GONE);
            } else {
                ((View) viewHolder.guests.getParent()).setVisibility(View.VISIBLE);
                viewHolder.guests.setText(personList(guests));
            }
        } else {
            ((View) viewHolder.guests.getParent()).setVisibility(View.GONE);
            ((View) viewHolder.hosts.getParent()).setVisibility(View.GONE);
        }

        viewHolder.approvedAt.setText(timeFormatter.format(s.approved_at));
    }

    @Override
    public int getItemCount() {
        return mSchedule.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.program_announce)
        TextView programAnnounce;
        @BindView(R.id.approved_at_time)
        TextView approvedAt;
        @BindView(R.id.program_title)
        TextView programTitle;
        @BindView(R.id.guests)
        TextView guests;
        @BindView(R.id.hosts)
        TextView hosts;

        public ViewHolder(View v) {
            super(v);

            ButterKnife.bind(this, v);

            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(v1 -> {
                int id = (int) v1.getTag();
                if (id >= 0) {
                    Intent i = new Intent(v1.getContext(), ProgramDetailActivity.class);
                    i.putExtra("id", id);
                    v1.getContext().startActivity(i);
                }
            });
        }

        public void setId(int id) {
            itemView.setTag(id);
        }
    }
}
