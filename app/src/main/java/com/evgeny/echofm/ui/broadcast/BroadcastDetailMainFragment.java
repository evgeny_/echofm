package com.evgeny.echofm.ui.broadcast;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.evgeny.echofm.R;
import com.evgeny.echofm.proto.Broadcast;
import com.evgeny.echofm.proto.Person;
import com.evgeny.echofm.ui.BaseFragment;
import com.evgeny.echofm.utils.DrawableUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

public class BroadcastDetailMainFragment extends BaseFragment {

    @BindView(R.id.bcast_title)
    TextView mBcastTitle;

    @BindView(R.id.program_title)
    TextView mProgramTitle;

    @BindView(R.id.approved_at_date)
    TextView approvedDateView;

    @BindView(R.id.approved_at_time)
    TextView approvedTimeView;

    @BindView(R.id.bcast_body)
    TextView mBodyView;

    @BindView(R.id.hosts)
    LinearLayout mHostsLayout;

    @BindView(R.id.guests)
    LinearLayout mGuestsLayout;

    private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    private Unbinder unbinder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRetainInstance(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.broadcast_detail_content, container, false);
        unbinder = ButterKnife.bind(this, view);

        mBodyView.setCustomSelectionActionModeCallback(new android.view.ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                mode.setTitle(null);
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.menu_broadcast_detail, menu);
                DrawableUtils.tintMenuItems(menu, ContextCompat.getColor(getActivity(), R.color.colorPrimaryIcon));
                return true;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                // Remove the "select all" option
                menu.removeItem(android.R.id.selectAll);
                // Remove the "cut" option
                menu.removeItem(android.R.id.cut);
                // Remove the "copy all" option
                menu.removeItem(android.R.id.copy);
                return true;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_share:
                        CharSequence selectedText = com.evgeny.echofm.utils.TextUtils.getSelectedText(mBodyView);
                        com.evgeny.echofm.utils.TextUtils.shareText(getActivity(), selectedText.toString());
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode mode) {

            }
        });

        if (getActivity() instanceof BroadcastDetailActivity) {
            ((BroadcastDetailActivity) getActivity()).getBroadcastObservable()
                    .compose(this.bindToLifecycle())
                    .subscribe(this::bindData, throwable -> {
                        Timber.w(throwable, "broadcast request error");
                    });
        }
        return view;
    }

    /**
     * Bind broadcast values to layout views.
     *
     * @param broadcast broadcast object
     */
    private void bindData(Broadcast broadcast) {
        if (!TextUtils.isEmpty(broadcast.program.title)) {
            mProgramTitle.setText(broadcast.program.title);
            mProgramTitle.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(broadcast.title)) {
            mBcastTitle.setText(Jsoup.parse(broadcast.title).text());
            mBcastTitle.setVisibility(View.VISIBLE);
        }

        if (!TextUtils.isEmpty(broadcast.body)) {
            Document document = Jsoup.parse(broadcast.body);
            mBodyView.setText(Html.fromHtml(document.select("p").outerHtml()));
            mBodyView.setVisibility(View.VISIBLE);
        }

        try {
            DateFormat approved = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.getDefault());
            Date d = approved.parse(broadcast.approved_at);
            approvedDateView.setText(dateFormatter.format(d));
            approvedTimeView.setText(timeFormatter.format(d));
        } catch (ParseException e) {
            Timber.e(e, "parse date(%s) of bcast id = %s failed ", broadcast.approved_at, broadcast.id);
        }


        if (broadcast.hosts.size() > 0) {
            for (Person person : broadcast.hosts) {
                addPerson(person, true, mHostsLayout);
            }
        }

        if (broadcast.guests.size() > 0) {
            for (Person person : broadcast.guests) {
                addPerson(person, false, mGuestsLayout);
            }
        }
    }

    private void addPerson(Person person, boolean isHost, ViewGroup parent) {
        LinearLayout ll = (LinearLayout) LayoutInflater.from(getActivity()).inflate(R.layout.layout_person, parent, false);
        ((TextView) ll.findViewById(R.id.name)).setText(getString(R.string.full_name, person.i, person.f));
        ((TextView) ll.findViewById(R.id.role)).setText(isHost ? getString(R.string.host) : getString(R.string.guest));
        ((TextView) ll.findViewById(R.id.position)).setText(person.position);

        if (person.avatar != null) {
            Glide.with(this).load(person.avatar.url)
                    .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                    .into((ImageView) ll.findViewById(R.id.avatar));
        }
        parent.addView(ll);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        unbinder.unbind();
    }
}
