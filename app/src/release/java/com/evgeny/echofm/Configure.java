package com.evgeny.echofm;

import android.app.Application;
import okhttp3.OkHttpClient;

public class Configure {

    public static void configStetho(Application application) {
        // stetho is disabled in release build
    }

    public static void addStethoInterceptor(OkHttpClient.Builder builder) {
        // stetho is disabled in release build
    }
}
