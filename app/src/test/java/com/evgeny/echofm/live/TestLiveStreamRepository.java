package com.evgeny.echofm.live;

import java.util.ArrayList;
import java.util.List;

public class TestLiveStreamRepository implements LiveStreamRepository {
    private LiveStream stream = new LiveStream(0, "test_url", "test_stream");

    @Override
    public void persistSelected(LiveStream liveStream) {
        stream = liveStream;
    }

    @Override
    public LiveStream retrieveSelected() {
        return stream;
    }

    @Override
    public List<LiveStream> getSourceList() {
        List<LiveStream> sources = new ArrayList<>(3);
        sources.add(new LiveStream(0, "test_url_0", "test_stream_0"));
        sources.add(new LiveStream(1, "test_url_1", "test_stream_1"));
        sources.add(new LiveStream(2, "test_url_2", "test_stream_2"));
        return sources;
    }
}
