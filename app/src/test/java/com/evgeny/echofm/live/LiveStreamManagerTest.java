package com.evgeny.echofm.live;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class LiveStreamManagerTest {

    private final LiveStreamManager liveStreamManager;

    public LiveStreamManagerTest() {
        TestLiveStreamRepository streamRepo = new TestLiveStreamRepository();
        liveStreamManager = new LiveStreamManager(streamRepo);
    }

    @Test
    public void getStreams() {
        List<LiveStream> streams = liveStreamManager.getStreams();
        assertEquals("different array length", streams.size(), 3);
    }

    @Test
    public void getSelectedStream() {
        LiveStream selectedStream = liveStreamManager.getSelectedStream();
        LiveStream testStream = new LiveStream(0, "test_url", "test_stream");

        assertEquals("selected stream is not the same", testStream, selectedStream);
    }

    @Test
    public void getNameArray() {
        CharSequence[] testArray = {"test_stream_0", "test_stream_1", "test_stream_2"};
        CharSequence[] nameArray = liveStreamManager.getNameArray();

        assertArrayEquals("names are different", testArray, nameArray);
    }

    @Test
    public void memorizeStream() {
        liveStreamManager.memorizeStream(new LiveStream(0, "test_url_31", "test_stream_31"));
        LiveStream selectedStream = liveStreamManager.getSelectedStream();

        assertEquals("stream wasnt updated",
                new LiveStream(0, "test_url_31", "test_stream_31"), selectedStream);
    }
}