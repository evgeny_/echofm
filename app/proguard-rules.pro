# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in D:\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-dontoptimize
-dontobfuscate

-keepattributes *Annotation*
-keepattributes EnclosingMethod
-keepattributes InnerClasses
-keepattributes Signature
-keepattributes Exceptions
-keepattributes SourceFile,LineNumberTable

-dontwarn javax.annotation.**
-dontwarn java.lang.reflect**

# ECHO MSK - MODEL
-keep class com.evgeny.echofm.model.** { *; }
-keep class com.evgeny.echofm.network.** { *; }

# GLIDE
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
#-keepresourcexmlelements manifest/application/meta-data@value=GlideModule

# RETROFIT
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
    @retrofit2.http.* <methods>;
}

#BUTTERKNIFE
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }

-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}

-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

#DAGGER
-dontwarn dagger.internal.**
-dontwarn dagger.producers.internal.**
-dontwarn dagger.shaded.auto.common.**

# Retrolambda
-dontwarn java.lang.invoke.*

# SimpleXML
-dontwarn com.bea.xml.stream.**
-dontwarn javax.xml.**

-keep public class org.simpleframework.**{ *; }
-keep class org.simpleframework.xml.**{ *; }
-keep class org.simpleframework.xml.core.**{ *; }
-keep class org.simpleframework.xml.util.**{ *; }

#-keep public class com.evgeny.model.**{ *; }

-keepattributes ElementList, Root
-keepclassmembers class * {
    @org.simpleframework.xml.* *;
}

# JODA
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }

# OkHTTP
-dontwarn okio.**
#-dontwarn okhttp3.**
-dontwarn org.conscrypt.**
#-keepnames class okhttp3.internal.publicsuffix.PublicSuffixDatabase

#EventBus
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# stetho
-keep class com.facebook.stetho.** {*;}

#RxJava
-dontwarn sun.misc.Unsafe
-dontwarn org.w3c.dom.bootstrap.DOMImplementationRegistry

#wire
-keep class com.squareup.wire.** { *; }
-keep class com.evgeny.echofm.proto.** { *; }